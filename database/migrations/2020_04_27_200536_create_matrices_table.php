<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->unsignedBigInteger('tipo_matriz_id');
            $table->boolean('asociacion_crm')->default(false);
            $table->string('grupo')->default('Master');
            $table->unsignedBigInteger('crm_id')->nullable();
            $table->unsignedBigInteger('rango_total_id');
            $table->unsignedBigInteger('tipo_peso_id');
            $table->unsignedBigInteger('usuario_creador_id');
            $table->unsignedBigInteger('usuario_actualizacion_id');
            $table->boolean('tiene_encuesta')->default(false);
            $table->unsignedBigInteger('encuesta_id')->nullable();
            $table->boolean('activo')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrices');
    }
}
