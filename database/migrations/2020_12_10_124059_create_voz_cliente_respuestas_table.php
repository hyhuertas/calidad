<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVozClienteRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voz_cliente_respuestas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('auditoria_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('voz_cliente_pregunta_id');
            $table->unsignedBigInteger('voz_cliente_formulario_id');
            $table->string('respuesta')->nullable();
            $table->unsignedBigInteger('respuesta_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voz_cliente_respuestas');
    }
}
