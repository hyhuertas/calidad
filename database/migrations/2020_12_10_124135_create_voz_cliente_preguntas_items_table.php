<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVozClientePreguntasItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voz_cliente_preguntas_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('voz_cliente_pregunta_id');
            $table->string('nombre');
            $table->boolean('otra_respuesta')->default(false);
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voz_cliente_preguntas_items');
    }
}
