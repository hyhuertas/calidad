<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncuestaPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encuesta_preguntas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->boolean('activo')->default(true);
            $table->unsignedBigInteger('select_id')->nullable();
            $table->boolean('aplica_select')->default(false);
            $table->boolean('aplica_comentario')->default(false);
            $table->boolean('not_null_select')->default(false);
            $table->boolean('not_null_cometario')->default(false);
            $table->unsignedBigInteger('usuario_creador_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encuesta_preguntas');
    }
}
