<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatrizAuditoriaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriz_auditoria_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('matriz_auditoria_id');
            $table->unsignedBigInteger('modulo_id');
            $table->integer('nivel')->default(1);
            $table->integer('padre_id')->nullable();
            $table->boolean('cumple')->default(false);
            $table->boolean('no_cumple')->default(false);
            $table->boolean('no_aplica')->default(false);
            $table->boolean('si')->default(false);
            $table->boolean('no')->default(false);
            $table->unsignedBigInteger('tipo_matriz_id');
            $table->integer('puntaje')->nullable();
            $table->integer('puntaje_obtenido')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz_auditoria_items');
    }
}
