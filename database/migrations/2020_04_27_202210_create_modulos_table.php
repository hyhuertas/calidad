<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('peso')->nullable();
            $table->boolean('premium')->default(false);
            $table->string('sigla');
            $table->smallInteger('nivel')->default(1);
            $table->unsignedBigInteger('padre_id')->nullable();
            $table->boolean('activo')->default(true);
            $table->unsignedBigInteger('usuario_creador_id');
            $table->unsignedBigInteger('usuario_actualizacion_id');
            $table->unsignedBigInteger('matriz_id');
            $table->boolean('error_critico')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}
