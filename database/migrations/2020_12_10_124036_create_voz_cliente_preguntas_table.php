<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVozClientePreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voz_cliente_preguntas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipo_pregunta_id');
            $table->unsignedBigInteger('tipo_pregunta_item_id')->nullable();
            $table->string('pregunta');
            $table->boolean('requerida')->default(true);
            $table->boolean('otra_respuesta')->default(false);
            $table->unsignedBigInteger('user_id');
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voz_cliente_preguntas');
    }
}
