<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatrizAuditoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriz_auditorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('asesor_id');
            $table->string('cedula_asesor');
            $table->date('fecha_llamada');
            $table->string('tlf_auditados');
            $table->string('hora_contacto');
            $table->string('ucid');
            $table->string('grabacion_minutos', 5);
            $table->string('grabacion_segundos', 5);
            $table->unsignedBigInteger('tipo_monitoreo_id');
            $table->unsignedBigInteger('tipo_servicio_id');
            $table->unsignedBigInteger('tipo_auditoria_id');
            $table->unsignedBigInteger('solucion_primer_contacto_id');
            $table->unsignedBigInteger('razon_solucion_id')->nullable();
            $table->text('porque_no')->nullable();
            $table->integer('seleccionar_dia')->default(0);
            $table->unsignedBigInteger('ojt_id');
            $table->unsignedBigInteger('matriz_id');
            $table->smallInteger('rol_auditor');
            $table->unsignedBigInteger('auditor_id');
            $table->text('observacion_auditor')->nullable();
            $table->string('total_punaje');
            $table->text('observacion_encuesta')->nullable();
            $table->unsignedBigInteger('auditor_retroalimentacion_id')->nullable();
            $table->unsignedBigInteger('supervisor_retroalimentacion_id')->nullable();
            $table->dateTime('fecha_retroalimentacion_auditor')->nullable();
            $table->dateTime('fecha_retroalimentacion_supervisor')->nullable();
            $table->text('retroalimentacion_auditor')->nullable();
            $table->text('compromiso_calidad')->nullable();
            $table->text('retroalimentacion_supervisor')->nullable();
            $table->text('compromiso')->nullable();
            $table->dateTime('fecha_compromiso')->nullable();
            $table->unsignedBigInteger('encuesta_id')->nullable();
            $table->string('maximo_puntaje');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz_auditorias');
    }
}
