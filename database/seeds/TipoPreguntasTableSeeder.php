<?php

use Illuminate\Database\Seeder;

class TipoPreguntasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipo_preguntas')->delete();
        
        \DB::table('tipo_preguntas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Respuesta corta',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:22',
                'updated_at' => '2020-12-10 13:08:22',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Selección múltiple',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:22',
                'updated_at' => '2020-12-10 13:08:22',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Escala lineal',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:23',
                'updated_at' => '2020-12-10 13:08:23',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Fecha',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:23',
                'updated_at' => '2020-12-10 13:08:23',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Hora',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:24',
                'updated_at' => '2020-12-10 13:08:24',
            ),
        ));
        
        
    }
}