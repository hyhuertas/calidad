<?php

use Illuminate\Database\Seeder;

class TipoPreguntasItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipo_preguntas_items')->delete();
        
        \DB::table('tipo_preguntas_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tipo_pregunta_id' => 3,
                'nombre' => '1 a 5',
                'regla' => '1to5',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:22',
                'updated_at' => '2020-12-10 13:08:22',
            ),
            1 => 
            array (
                'id' => 2,
                'tipo_pregunta_id' => 3,
                'nombre' => '1 a 10',
                'regla' => '1to10',
                'activo' => 1,
                'created_at' => '2020-12-10 13:08:22',
                'updated_at' => '2020-12-10 13:08:22',
            ),
        ));
        
        
    }
}