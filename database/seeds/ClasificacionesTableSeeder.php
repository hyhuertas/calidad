<?php

use Illuminate\Database\Seeder;

class ClasificacionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificaciones')->delete();
        
        \DB::table('clasificaciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Tipo de Matriz',
                'descripcion' => 'Tipo de Matriz',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Rangos de Valor',
                'descripcion' => 'Rangos de Valor',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Tipo de Monitoreo',
                'descripcion' => 'Tipo de Monitoreo',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Tipo de Servicio',
                'descripcion' => 'Tipo de Servicio',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Tipo de Auditoria',
                'descripcion' => 'Tipo de Auditoria',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Peso de Matriz',
                'descripcion' => 'Peso de Matriz',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Solución Primer Contacto',
                'descripcion' => 'Solución Primer Contacto',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Ojt',
                'descripcion' => 'Ojt',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Razón de no solucion',
                'descripcion' => ' <v-textarea',
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
        ));
        
        
    }
}