<?php

use Illuminate\Database\Seeder;

class ClasificacionItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clasificacion_items')->delete();
        
        \DB::table('clasificacion_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Cumplimiento',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 1,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Valor',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 1,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Peso en Items',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Peso en Módulos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 6,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'De 0 a 5',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'De 0 a 100',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'De 0 a 130',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Remoto',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 3,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'En vivo',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 3,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'Side by side',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 3,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'Prepago',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 4,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'Pospago',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 4,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'No Aplica',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 4,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'Interna',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 5,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'Cliente',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 5,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            15 => 
            array (
                'id' => 16,
                'nombre' => 'Si',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 7,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            16 => 
            array (
                'id' => 17,
                'nombre' => 'No',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 7,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            17 => 
            array (
                'id' => 18,
                'nombre' => 'Si',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            18 => 
            array (
                'id' => 19,
                'nombre' => 'No',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 8,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            19 => 
            array (
                'id' => 20,
                'nombre' => 'Agente',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            20 => 
            array (
                'id' => 21,
                'nombre' => 'Proceso Cos',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            21 => 
            array (
                'id' => 22,
                'nombre' => 'Proceso Cliente',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            22 => 
            array (
                'id' => 23,
                'nombre' => 'Usuario Final',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 9,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            23 => 
            array (
                'id' => 24,
                'nombre' => 'De 1 a 5',
                'nivel' => 1,
                'padre_id' => NULL,
                'clasificacion_id' => 2,
                'activo' => 1,
                'usuario_creador_id' => 59,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
        ));
        
        
    }
}