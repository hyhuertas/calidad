<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'idrol' => 1,
                'descripcion' => 'Creador de Matrices',
                'rol' => 1,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            1 => 
            array (
                'idrol' => 2,
                'descripcion' => 'Asesor',
                'rol' => 2,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            2 => 
            array (
                'idrol' => 3,
                'descripcion' => 'Control',
                'rol' => 3,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            3 => 
            array (
                'idrol' => 4,
                'descripcion' => 'Auditor',
                'rol' => 4,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            4 => 
            array (
                'idrol' => 5,
                'descripcion' => 'Supervisor',
                'rol' => 5,
                'created_at' => '2020-04-28 09:00:00',
                'updated_at' => '2020-04-28 09:00:00',
            ),
            // 5 => 
            // array (
            //     'idrol' => 6,
            //     'descripcion' => 'Calidad',
            //     'rol' => 6,
            //     'created_at' => '2020-04-28 09:00:00',
            //     'updated_at' => '2020-04-28 09:00:00',
            // ),
        ));
        
        
    }
}