<v-app-bar
   elevation="2"
   :clipped-left="$vuetify.breakpoint.lgAndUp"
   app
   color="white"
   dark
   >
   <v-toolbar-title
      style="width: 85px"
      class="ml-0 pl-3"
      >
      <v-app-bar-nav-icon @click.stop="drawer = !drawer">
         <v-icon color="grey darken-3">mdi-menu</v-icon>
      </v-app-bar-nav-icon>
   </v-toolbar-title>
   {{--
   <v-text-field
      flat
      solo-inverted
      hide-details
      prepend-inner-icon="search"
      label="Search"
      class="hidden-sm-and-down"
      ></v-text-field>
   --}}
   <h1  class="grey--text text--darken-3 headline">
     @if (Session::get('numero_rol_aseg_bajas') == 1)
     Súper Administrador
     @endif
     @if (Session::get('numero_rol_aseg_bajas') == 2)
         Asesor
     @endif
     </h1>
   <v-spacer></v-spacer>
   {{-- <img class="mr-5" style="width: 174px" src="/img/logo_chilco.png"/> --}}
   <v-divider vertical></v-divider>
   <v-divider vertical></v-divider>
   
   
   {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
   </form> --}}
   {{--
   <span class="ml-5" >
      <a class="white--text" style="text-decoration:none" href="{{ route('logout') }}"
         onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
         <v-icon>mdi-logout</v-icon>
         Cerrar sesión
      </a>
   </span>
   --}}
</v-app-bar>
