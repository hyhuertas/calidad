


    <v-navigation-drawer
    class="menu_principal grey lighten-3"
    
    v-model="drawer"
    :clipped="$vuetify.breakpoint.lgAndUp"
    app
  >
    <v-list   dense shaped>
      <template v-for="item in items">
       
        <v-list-group
          v-if="item.children"
          :key="item.text"
          v-model="item.model"
          :prepend-icon="item.model ? item.icon : item['icon-alt']"
          append-icon=""
          
        >
          <template v-slot:activator>
            <v-list-item class="pa-0">
              <v-list-item-content>
                <v-list-item-title>
                  @{{ item.text }}
                </v-list-item-title>
              </v-list-item-content>
            </v-list-item>
          </template>
          <v-list-item
            v-for="(child, i) in item.children"
            :key="i"
            @click=""
            :to="child.ruta"
          >
            <v-list-item-action style="margin-right: 15px;
            margin-left: 20px;" v-if="child.icon">
              <v-icon size="15"> @{{ child.icon }}</v-icon>
            </v-list-item-action>
            <v-list-item-content>
              <v-list-item-title>
                @{{ child.text }}
              </v-list-item-title>
            </v-list-item-content>
          </v-list-item>
        </v-list-group>
        <v-list-item
        active-class="grey lighten-2 black--text"
        v-else
        :key="item.text"
        @click=""
        :to="item.ruta"
      >
        <v-list-item-action>
          <v-icon>@{{ item.icon }}</v-icon>
        </v-list-item-action>
        <v-list-item-content>
          <v-list-item-title class="text-capitalize">
            @{{ item.text }}
          </v-list-item-title>
        </v-list-item-content>
      </v-list-item>
    

      </template>
    </v-list>
  </v-navigation-drawer>

