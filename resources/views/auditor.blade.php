@extends('layoutAuditor')
@section('componentes_vue')
<v-content>
    <v-container  fluid
    fill-height >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
                <router-view></router-view>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>

    {{-- @include('theme.menu') --}}
    {{-- @include('theme.bar') --}}
@endsection
