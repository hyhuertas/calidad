<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">


    <!--Añadimos el css generado con webpack-->
    <style>
        *, 
        .v-application .display-1, 
        .v-application .display-2,
        .v-application .display-3,
        .v-application .display-4,
        .v-application .headline,
        .v-application .title,
        .v-application .subtitle-1,
        .v-application .subtitle-2,
        .v-application .body-1,
        .v-application .body-2,
        .v-application .caption,
        .v-application .overline
        {
            font-family: 'Open Sans', sans-serif!important;;
            /* font-family: 'Nunito', sans-serif!important; */
            /* font-family: 'PT Sans Narrow', sans-serif!important; */

        }
        .no-padding .v-toolbar__content {
            padding: 0 !important;
        }
        ::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    border-radius: 10px;
    background-color: #fff;
}

::-webkit-scrollbar {
    width: 8px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    border-radius: 10px;
    background-color: #fff;
}

::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    background-color: #eef0f2;
}

.v-text-field.v-input--dense .v-input__append-inner .v-input__icon>.v-icon, .v-text-field.v-input--dense .v-input__prepend-inner .v-input__icon>.v-icon {
    margin-top: 0px!important;
}

    </style>
</head>

<body>
    <div id="app" class="content">
        <!--La equita id debe ser app, como hemos visto en app.js-->
        <v-app style="width: 100%;" id="inspire">

            @yield('componentes_vue')

        </v-app>
    </div>
    <script src="{{asset('js/app-asesor.js')}}"></script>
    <!--Añadimos el js generado con webpack, donde se encuentra nuestro componente vuejs-->
</body>

</html>