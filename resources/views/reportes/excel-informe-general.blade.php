@php
$head = ($cabeceras);
@endphp  
<table>
    <thead>
        <tr>
            <td style="text-align:center; background:  #629ecf">IDmaster Consecutivo Único De Auditoría</td>
            <td style="text-align:center; background:  #629ecf">Auditor</td>
            <td style="text-align:center; background:  #629ecf">Fecha Creación</td>
            <td style="text-align:center; background:  #629ecf">Hora Creación</td>
            <td style="text-align:center; background:  #629ecf">Nombre Del Asesor Auditado</td>
            <td style="text-align:center; background:  #629ecf">Cedula Asesor Auditado</td>
            <td style="text-align:center; background:  #629ecf">Antigüedad Del Asesor Auditado</td>
            <td style="text-align:center; background:  #629ecf">Fecha Llamada Auditada</td>
            <td style="text-align:center; background:  #629ecf">Hora Contacto</td>
            <td style="text-align:center; background:  #629ecf">Número Telefono Auditado</td>
            <td style="text-align:center; background:  #629ecf">Tipo Monitoreo</td> 
            <td style="text-align:center; background:  #629ecf">Duración Llamada</td> 
            <td style="text-align:center; background:  #629ecf">Servicio</td> 
            <td style="text-align:center; background:  #629ecf">Ucid</td>           
            <td style="text-align:center; background:  #629ecf">Tipo Auditoría</td>
            <td style="text-align:center; background:  #629ecf">Solucion Primer Contacto</td>
            <td style="text-align:center; background:  #629ecf">Razon No Solucion</td>
            <td style="text-align:center; background:  #629ecf">Motivo No Solucion</td>
            <td style="text-align:center; background:  #629ecf">Ojt</td>
            <td style="text-align:center; background:  #629ecf">Dia</td>
            <td style="text-align:center; background:  #629ecf">Calificación Máxima</td>
            <td style="text-align:center; background:  #629ecf">Calificación Del Monitoreo</td>
            <td style="text-align:center; background:  #629ecf">Observación</td>
            <td style="text-align:center; background:  #629ecf">Estado Retroalimentacion</td>
            <td style="text-align:center; background:  #629ecf">Fecha Retroalimentacion Auditor</td>
            <td style="text-align:center; background:  #629ecf">Fecha Retroalimentacion Supervisor</td>
            <td style="text-align:center; background:  #629ecf">Hora Retroalimentacion Auditor</td>
            <td style="text-align:center; background:  #629ecf">Hora Retroalimentacion Supervisor</td>
            <td style="text-align:center; background:  #629ecf">Retroalimentacion Auditor</td>
            <td style="text-align:center; background:  #629ecf">Retroalimentacion Supervisor</td>
            <td style="text-align:center; background:  #629ecf">Fecha Compromiso</td>
            <td style="text-align:center; background:  #629ecf">Compromiso Calidad</td>
            <td style="text-align:center; background:  #629ecf">Compromiso Operación</td>
            {{-- @foreach($cabeceras as $keyMatriz => $matriz) --}}
            @foreach($cabeceras->modulos as $keyModulo => $modulo){{--MODULOS--}}
                    <td style="text-align:center; background:  #b7e988">{{$modulo['nombre']}}</td>  
                    {{-- <td >{{$abc['id']}}</td> --}}
                        @foreach($modulo->itemsModulo as $keyItem => $item){{--ITEMS--}}
                        <td style="text-align:center; background:  #b7e988">{{$item['nombre']}}</td>
                            @foreach($item->subItems as $keySubItem => $subItem){{--SUBITEMS--}}
                            <td style="text-align:center; background:  #b7e988">{{$subItem['nombre']}}</td>
                            @endforeach
                        @endforeach
            @endforeach

            {{-- @endforeach --}}
        </tr>
    </thead>
    <tbody>
        {{-- {{dd($gestiones)}} --}}
        @foreach($gestiones as $keyModulo => $gestion){{--GESTIONES--}}
        @php

        // dd($gestion->detalleAsesor->usuarioApp->fecha_creacion_usuario);



            $fecha_ingreso = 'N/A';
            

            if($gestion->detalleAsesor){

                $fecha_ingreso = Carbon\Carbon::parse($gestion->detalleAsesor->usuarioApp->fecha_creacion_usuario)->format('Y-m-d');

            //           $year = Carbon\Carbon::parse($gestion->detalleAsesor->usuarioApp->fecha_creacion_usuario)->format('Y');
            // $mes = Carbon\Carbon::parse($gestion->detalleAsesor->usuarioApp->fecha_creacion_usuario)->format('m');
            // $dia = Carbon\Carbon::parse($gestion->detalleAsesor->usuarioApp->fecha_creacion_usuario)->format('d');
            }

      
            
            // $year1 = Carbon\Carbon::now();
            // $year2 = $year1->format('Y');
            // $mes1 = Carbon\Carbon::now();
            // $mes2 = $mes1->format('m');
            // $dia1 = Carbon\Carbon::now();
            // $dia2 = $dia1->format('d');

            // $timestamp1 = mktime(0,0,0,$mes,$dia,$year);
            // $timestamp2 = mktime(4,12,0,$mes2,$dia2,$year2);

            // $segundos_diferencia = $timestamp1 - $timestamp2;

            // $dias_diferencia = $segundos_diferencia / (60*60*24);

            // $dias_diferencia = abs($dias_diferencia);

            // $dias_diferencia = number_format($dias_diferencia,0);
            // // dd($gestion->detalleRazon['nombre']);
        @endphp

             <tr> {{-- <td >{{$modulo['nombre']}}</td>   --}}
            {{-- <td >{{$abc['id']}}</td> --}}
                <td style="text-align:center; background: #c9e7ff">{{$gestion['id']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->detalleAuditor['nombre'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{  Carbon\Carbon::parse($gestion['created_at'])->format('Y-m-d')}}</td>
                <td style="text-align:center; background: #c9e7ff">{{  Carbon\Carbon::parse($gestion['created_at'])->format('H:i:s')}}</td>
                <td style="text-align:center; background: #c9e7ff">{{  $gestion->detalleAsesor ? $gestion->detalleAsesor['nombre'] : '' }}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['cedula_asesor']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $fecha_ingreso }}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_llamada']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['hora_contacto']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['tlf_auditados']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->detalleMonitoreo['nombre'] }}</td> 
                <td style="text-align:center; background: #c9e7ff">{{$gestion['grabacion_minutos']}}:{{$gestion['grabacion_segundos']}}</td> 
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->detalleServicio['nombre'] }}</td> 
                <td style="text-align:center; background: #c9e7ff">{{$gestion['ucid']}}</td>           
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->tipoAudioria['nombre'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->solucionContacto['nombre'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{ !empty($gestion->detalleRazon['nombre']) ? $gestion->detalleRazon['nombre'] : $gestion['nombre']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion['porque_no'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->detalleOjt['nombre'] }}</td>
                <td style="text-align:center; background: #c9e7ff"> {{ $gestion['seleccionar_dia'] > 0 ? $gestion['seleccionar_dia'] : 'N/A' }}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion->detalleMatriz->valor_peso_validacion['maximo'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{ $gestion['total_punaje'] }}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['observacion_auditor']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['auditor_retroalimentacion_id'] || $gestion['supervisor_retroalimentacion_id'] ? 'Con Retroalimentacion' : 'Sin Retroalimentacion'}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_retroalimentacion_auditor'] ? Carbon\Carbon::parse($gestion['fecha_retroalimentacion_auditor'])->format('Y-m-d') : ''}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_retroalimentacion_supervisor'] ? Carbon\Carbon::parse($gestion['fecha_retroalimentacion_supervisor'])->format('Y-m-d') : ''}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_retroalimentacion_auditor'] ? Carbon\Carbon::parse($gestion['fecha_retroalimentacion_auditor'])->format('H:i:s') : ''}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_retroalimentacion_supervisor'] ? Carbon\Carbon::parse($gestion['fecha_retroalimentacion_supervisor'])->format('H:i:s') : ''}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['retroalimentacion_auditor']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['retroalimentacion_supervisor']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['fecha_compromiso']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['compromiso_calidad']}}</td>
                <td style="text-align:center; background: #c9e7ff">{{$gestion['compromiso']}}</td>
                @foreach($gestion->itemModulos as $keyItem => $modulo){{--MODULOS--}}
                <td style="text-align:center; background:  #e4facf">{{is_null($modulo->detalleItem['peso']) ? $modulo['type_select'] : $modulo->detalleItem['peso']}}</td>
                    @foreach($gestion->itemItems as $keySubItem => $item){{--ITEMS--}}
                        @if($modulo['modulo_id'] == $item['padre_id'])
                        <td style="background:  #e4facf">{{$item['type_select']}}</td>
                                @foreach($gestion->itemSubItems as $keySubItem => $subitem){{--SUBITEMS--}}
                                    @if($item['modulo_id'] == $subitem['padre_id'])
                                    <td style="background:  #e4facf">{{$subitem['type_select']}}</td>
                                    @endif
                                @endforeach
                        @endif
                    @endforeach
                @endforeach 
                {{-- <td>
                {{json_encode($gestion['encuestaGenerada'])}}
                </td>  --}}
                @foreach($gestion['encuestaGenerada'] as $keyRespuesta => $respuesta)
                <td><strong>{{$respuesta['preguntaDetalle']['pregunta']}}</strong></td>
                <td> 
                    {{$respuesta['respuestSelect'] ?  $respuesta['respuestSelect']['nombre'] : ''}}
                    {{$respuesta['respuestSelect'] && $respuesta['respuesta'] ?  ' - ' : ''}}
                    {{$respuesta['respuesta']}}
                </td>
                @endforeach
             </tr>
    @endforeach
     
    </tbody>    
</table>