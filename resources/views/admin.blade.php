@extends('layoutAdmin')
@section('componentes_vue')
<v-content>
    <v-container class="pl-0 pr-0" fluid
    fill-height >
        <v-layout>
            <v-fade-transition fluid fill-height mode="out-in">
                <router-view></router-view>
            </v-fade-transition>
        </v-layout>
    </v-container>
</v-content>
<v-navigation-drawer permanent app expand-on-hover class="menu_nav">
    <v-list style="width: 100%;" nav dense >
        <v-list-item-group color="white">
      <v-list-item active-class="primary" link to="/admin">
        <v-list-item-icon>
          <v-icon>mdi-layers-triple</v-icon>
        </v-list-item-icon>
        <v-list-item-title class="font-weight-bold">Matriz</v-list-item-title>
      </v-list-item>
      <v-list-item active-class="primary" link to="/adm/voz-cliente">
        <v-list-item-icon>
          <v-icon>mdi-file-document-outline</v-icon>
        </v-list-item-icon>
        <v-list-item-title  class="font-weight-bold">Voz cliente</v-list-item-title>
      </v-list-item>
      <v-list-item active-class="primary" link to="/adm/calidad">
        <v-list-item-icon>
          <v-icon>mdi-thumb-up-outline</v-icon>
        </v-list-item-icon>
        <v-list-item-title  class="font-weight-bold">Calidad</v-list-item-title>
      </v-list-item>
        </v-list-item-group>
    </v-list>
  </v-navigation-drawer>
    {{-- @include('theme.menu') --}}
    {{-- @include('theme.bar') --}}
@endsection
