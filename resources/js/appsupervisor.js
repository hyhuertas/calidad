/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'



window.Vue = require('vue');



Vue.use(Vuetify)
Vue.use(VueRouter)
 
// Vue.component('supervisor-component', require('./components/supervisor/SupervisorComponent').default);
// Vue.component('supervisor-auditorias-component', require('./components/supervisor/SupervisorAuditoriasComponent').default);

// let supervisor_component = {
//     template: `<supervisor-component></supervisor-component>`
// }

// let supervisor_auditorias_component = {
//     template: `<supervisor-auditorias-component></supervisor-auditorias-component>`
// }


Vue.component('inicio-component', require('./components/supervisor/inicioComponent').default);
Vue.component('matrizauditar-component', require('./components/supervisor/matrizAuditarComponent').default);
Vue.component('matrizretroalimentar-component', require('./components/supervisor/matrizRetroalimentarComponent').default);
Vue.component('verauditorias-component', require('./components/supervisor/VerAuditoriasComponent').default);
Vue.component('informes-component', require('./components/informes/InformesComponent').default);


let informes_component = {
    template: `<informes-component></informes-component>`
}
let inicio_component = {
    template: `<inicio-component></inicio-component>`
}
let matrizauditar_component = {
    template: `<matrizauditar-component></matrizauditar-component>`
}
let matrizretroalimentar_component = {
    template: `<matrizretroalimentar-component></matrizretroalimentar-component>`
}
let verauditorias_component = {
    template: `<verauditorias-component></verauditorias-component>`
}




const router = new VueRouter({
    routes: [{
        path: '/supervisor',
        name: 'Form',
        component: inicio_component,
    },
   {
        path: '/supervisor/matrizauditar/:idmatriz',
        name: 'Matriz Auditar',
        component: matrizauditar_component,
    },
    {
        path: '/supervisor/matrizretroalimentar/:idmatriz',
        name: 'Matriz Retroalimentar',
        component: matrizretroalimentar_component,
    },
    {
        path: '/supervisor/verauditorias/:idmatriz',
        name: 'Matriz Retroalimentar',
        component: verauditorias_component,
    },
    {
        path: '/supervisor/informes/:idmatriz',
        name: 'Matriz Reporteria',
        component: informes_component,
    },
    // {
    //     path: '/reporte',
    //     name: 'Reporte gestiones',
    //     component: reporte_component,
    // }
    ],
    mode: 'history'
});

// const router = new VueRouter({
//     routes: [
//     {
//         path: '/supervisor',
//         name: 'supervisor',
//         component: supervisor_component,
//     },
//     {
//         path: '/supervisor/auditorias',
//         name: 'supervisor-auditorias',
//         component: supervisor_auditorias_component,
//     },
//     {
//         path: '/supervisor/auditorias/:id',
//         name: 'supervisor-auditorias',
//         component: supervisor_auditorias_component,
//     }
//     ],
//     mode: 'history'
// });

const app = new Vue({
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi', // default - only for display purposes
          },
        theme: {
            themes: {
                light: {
                    primary: '#D52B1E', // rojo
                    secondary: '#1B506F', // azul
                    accent: '#2D968B',// verde
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
    data: () => ({
        drawer: true,
        items: [
            { icon: 'mdi-format-list-text', text: 'Inicio', ruta: '/' },
            { icon: 'mdi-file-export', text: 'Generar informe', ruta: '/generarInforme' },
            // { icon: 'dvr', text: 'Reportes', ruta: '/reporte'},
            // { icon: 'library_add', text: 'Mis Gestiones', ruta: '/mis_gestiones' },
        ],

      })
});
