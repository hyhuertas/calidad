/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'



window.Vue = require('vue');



Vue.use(Vuetify)
Vue.use(VueRouter)
 
Vue.component('inicio-component', require('./components/auditor/inicioComponent').default);
Vue.component('matrizauditar-component', require('./components/auditor/matrizAuditarComponent').default);
Vue.component('matrizretroalimentar-component', require('./components/auditor/matrizRetroalimentarComponent').default);
Vue.component('informes-component', require('./components/informes/InformesComponent').default);


let informes_component = {
    template: `<informes-component></informes-component>`
}

let inicio_component = {
    template: `<inicio-component></inicio-component>`
}
let matrizauditar_component = {
    template: `<matrizauditar-component></matrizauditar-component>`
}
let matrizretroalimentar_component = {
    template: `<matrizretroalimentar-component></matrizretroalimentar-component>`
}



const router = new VueRouter({
    routes: [{
        path: '/auditor',
        name: 'Form',
        component: inicio_component,
    },
   {
        path: '/auditor/matrizauditar/:idmatriz',
        name: 'Matriz Auditar',
        component: matrizauditar_component,
    },
    {
        path: '/auditor/matrizretroalimentar/:idmatriz',
        name: 'Matriz Retroalimentar',
        component: matrizretroalimentar_component,
    },
    {
        path: '/auditor/informes/:idmatriz',
        name: 'Auditor Reportes',
        component: informes_component,
    },
    // {
    //     path: '/reporte',
    //     name: 'Reporte gestiones',
    //     component: reporte_component,
    // }
    ],
    mode: 'history'
});

const app = new Vue({
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi', // default - only for display purposes
          },
        theme: {
            themes: {
                light: {
                    primary: '#D52B1E', // rojo
                    secondary: '#1B506F', // azul
                    accent: '#2D968B',// verde
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
    data: () => ({
        drawer: true,
        items: [
            { icon: 'mdi-format-list-text', text: 'Inicio', ruta: '/' },
            { icon: 'mdi-file-export', text: 'Generar informe', ruta: '/generarInforme' },
            // { icon: 'dvr', text: 'Reportes', ruta: '/reporte'},
            // { icon: 'library_add', text: 'Mis Gestiones', ruta: '/mis_gestiones' },
        ],

      })
});
