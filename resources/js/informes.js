/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'



window.Vue = require('vue');



Vue.use(Vuetify)
Vue.use(VueRouter)
 
Vue.component('informes-component', require('./components/informes/InformesComponent').default);


let informes_component = {
    template: `<informes-component></informes-component>`
}

const router = new VueRouter({
    routes: [
    {
        path: '/informes',
        name: 'Form',
        component: informes_component,
    }
    ],
    mode: 'history'
});

const app = new Vue({
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi', // default - only for display purposes
          },
        theme: {
            themes: {
                light: {
                    primary: '#D52B1E', // rojo
                    secondary: '#1B506F', // azul
                    accent: '#2D968B',// verde
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
    data: () => ({
        drawer: true,
        items: [
            { icon: 'mdi-format-list-text', text: 'Inicio', ruta: '/' },
            { icon: 'mdi-file-export', text: 'Generar informe', ruta: '/generarInforme' },
            // { icon: 'dvr', text: 'Reportes', ruta: '/reporte'},
            // { icon: 'library_add', text: 'Mis Gestiones', ruta: '/mis_gestiones' },
        ],

      })
});
