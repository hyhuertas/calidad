/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'



window.Vue = require('vue');



Vue.use(Vuetify)
Vue.use(VueRouter)
 
Vue.component('inicio-component', require('./components/administrador/inicioComponent').default);
Vue.component('voz-cliente-component', require('./components/administrador/vozClienteComponent').default);
Vue.component('voz-cliente-formulario-component', require('./components/administrador/vozClienteFormularioComponent').default);
Vue.component('voz-cliente-formulario-view-component', require('./components/administrador/vozClienteFormularioViewComponent').default);
Vue.component('calidad-component', require('./components/administrador/calidadComponent').default);

Vue.component('matrizconfig-component', require('./components/administrador/matrizConfigComponent').default);
Vue.component('control-component', require('./components/control/ControlComponent').default);
Vue.component('control-campaña-component', require('./components/control/ControlCampañaComponent').default);
Vue.component('informes-component', require('./components/informes/InformesComponent').default);


let informes_component = {
    template: `<informes-component></informes-component>`
}

let inicio_component = {
    template: `<inicio-component></inicio-component>`
}
let voz_cliente_component = {
    template: `<voz-cliente-component></voz-cliente-component>`
}
let voz_cliente_formulario_component = {
    template: `<voz-cliente-formulario-component></voz-cliente-formulario-component>`
}
let voz_cliente_formulario_view_component = {
    template: `<voz-cliente-formulario-view-component></voz-cliente-formulario-view-component>`
}
let calidad_component = {
    template: `<calidad-component></calidad-component>`
}

let matrizconfig_component = {
    template: `<matrizconfig-component></matrizconfig-component>`
}

let control_component = {
    template: `<control-component></control-component>`
}

let control_campaña_component = {
    template: `<control-campaña-component></control-campaña-component>`
}

const router = new VueRouter({
    routes: [{
        path: '/admin',
        name: 'HOME ADMIN',
        component: inicio_component,
    },{
        path: '/adm/voz-cliente',
        name: 'VOZ CLIENTE',
        component: voz_cliente_component,
    },{
        path: '/adm/voz-cliente/formulario',
        name: 'VOZ CLIENTE FORMULARIO',
        component: voz_cliente_formulario_component,
    },{
        path: '/adm/voz-cliente/formulario/:idformulario',
        name: 'VOZ CLIENTE FORMULARIO',
        component: voz_cliente_formulario_view_component,
    },{
        path: '/adm/calidad',
        name: 'ADMIN CALIDAD',
        component: calidad_component,
    },
   {
        path: '/admin/matrizconfig/:idmatriz',
        name: 'ADMIN CONFIG MATRIZ',
        component: matrizconfig_component,
    },
    {
        path: '/admin/informes/:idmatriz',
        name: 'Reporteria Admin',
        component: informes_component,
    },
    {
        path: '/control',
        name: 'HOME CONTROL',
        component: control_component,
    },
    {
        path: '/control/campaña',
        name: 'CONTROL CAMPAÑA',
        component: control_campaña_component,
    },
    {
        path: '/control/campaña/:id',
        name: 'control-campaña',
        component: control_campaña_component,
    },
    // {
    //     path: '/reporte',
    //     name: 'Reporte gestiones',
    //     component: reporte_component,
    // }
    ],
    mode: 'history'
});

const app = new Vue({
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi', // default - only for display purposes
          },
        theme: {
            themes: {
                light: {
                    primary: '#D52B1E', // rojo
                    secondary: '#1B506F', // azul
                    accent: '#2D968B',// verde
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
    data: () => ({
        drawer: true,
        items: [
            { icon: 'mdi-format-list-text', text: 'Inicio', ruta: '/' },
            { icon: 'mdi-file-export', text: 'Generar informe', ruta: '/generarInforme' },
            // { icon: 'dvr', text: 'Reportes', ruta: '/reporte'},
            // { icon: 'library_add', text: 'Mis Gestiones', ruta: '/mis_gestiones' },
        ],

      })
});
