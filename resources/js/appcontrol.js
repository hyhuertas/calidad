/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'



window.Vue = require('vue');



Vue.use(Vuetify)
Vue.use(VueRouter)

function dynamicPropsFn(route) {
    const now = new Date()
    return {
        id: (now.getFullYear() + parseInt(route.params.id)) + '!'
    }
}

Vue.component('control-component', require('./components/control/ControlComponent').default);
Vue.component('control-campaña-component', require('./components/control/ControlCampañaComponent').default);


let control_component = {
    template: `<control-component></control-component>`
}

let control_campaña_component = {
    props: ['id'],
    template: `<control-campaña-component></control-campaña-component>`
}

const router = new VueRouter({
    routes: [
        {
            path: '/control',
            name: 'Form',
            component: control_component,
        },
        {
            path: '/control/campaña',
            name: 'Form',
            component: control_campaña_component,
        },
        {
            path: '/control/campaña/:id',
            name: 'control-campaña',
            component: control_campaña_component,
            props: {id: true}
        },
        // {
        //     path: '/reporte',
        //     name: 'Reporte gestiones',
        //     component: reporte_component,
        // }
    ],
    mode: 'history'
});

const app = new Vue({
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi', // default - only for display purposes
        },
        theme: {
            themes: {
                light: {
                    primary: '#D52B1E', // rojo
                    secondary: '#1B506F', // azul
                    accent: '#2D968B',// verde
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
    data: () => ({
        drawer: true,
        items: [
            { icon: 'mdi-format-list-text', text: 'Inicio', ruta: '/' },
            { icon: 'mdi-file-export', text: 'Generar informe', ruta: '/generarInforme' },
            // { icon: 'dvr', text: 'Reportes', ruta: '/reporte'},
            // { icon: 'library_add', text: 'Mis Gestiones', ruta: '/mis_gestiones' },
        ],

    })
});
