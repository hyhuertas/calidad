export default {
    data() {
        return {
            rules_form: {
                requerido: v => !!v || "Este campo es requerido",
                //!/^([0-9])*$/
                numeros: v => /^([0-9.])*$/.test(v) || "Este campo debe ser númerico",

                alfanumerico: v =>
                    /^([0-9A-Za-z])*$/.test(v) ||
                    "Este campo no permite caracteres especiales",
                sololetras: v =>
                    /^([ A-Za-zñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ])*$/.test(
                        v
                    ) || "Este campo no permite números ni caracteres especiales",
                maximo255caracteres: v =>
                    (v && v.length <= 255) ||
                    "Este campo no debe superar los 255 caracteres",
                maximo150caracteres: v =>
                    (v && v.length <= 150) ||
                    "Este campo no debe superar los 150 caracteres",
                maximo10caracterestel: v =>
                    (v && v.length <= 10) ||
                    "Este campo no debe superar los 10 caracteres",
                maximo3caracterestel: v =>
                    (v && v.length <= 3) ||
                    "Este campo no debe superar los 3 caracteres",
                maximo5caracterestel: v =>
                    (v && v.length <= 5) ||
                    "Este campo no debe superar los 5 caracteres",

                email: v => /.+@.+\..+/.test(v) || "Correo electrónico no válido",

                numerospass: v => /^(?=.*\d)/.test(v) || 'Debe contener al menos un valor númerico',
                letraspass: v => /^(?=.*[a-z])(?=.*[A-Z])/.test(v) || 'Debe contener letras mayusculas y minusculas',
                minpass: v => /^.{6,15}$/.test(v) || 'Debe tener entre 6 y 15 caracteres'
            },
        }
    },
}
