<?php

namespace App\Http\Controllers;

use App\Models\Matriz;
use App\Models\TipoPregunta;
use App\Models\VozClienteFormulario;
use App\Models\VozClienteFormularioPregunta;
use Illuminate\Http\Request;
use DB;
use Session;

class VozClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validarDisponibleFormulario(Request $request)
    {
        $validarFormulario = Matriz::where('encuesta_id', $request->id_formulario)
            ->count();
        if ($validarFormulario > 0) {
            return ["eliminar" => false];
        } else {
            return ["eliminar" => true];
        }
    }
    public function validarDisponiblePregunta(Request $request)
    {
        $validarPregunta = VozClienteFormularioPregunta::where('voz_cliente_pregunta_id', $request->id_pregunta)
            ->count();
        if ($validarPregunta > 0) {
            return ["eliminar" => false];
        } else {
            return ["eliminar" => true];
        }
    }
    public function verMatricesAsignadas($id)
    {
        return Matriz::where('encuesta_id', $id)->get();
    }
    public function getTiposPreguntas()
    {
        return TipoPregunta::with('itemsTipoPregunta')->where('activo', true)->get();
    }
    public function index(Request $request)
    {

        // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');


        $inicio = -1;
        if ($request->rowsPerPage && $request->page) {
            $inicio = ($request->page - 1) * $request->rowsPerPage;
        };

        $formulario['total_registros'] = VozClienteFormulario::where('activo', true);
        if ($request->search) {
            $formulario['total_registros'] = VozClienteFormulario::where('nombre', 'like', '%' . $request->search . '%');
        }
        $formulario['total_registros'] = $formulario['total_registros']->count();


        $formulario['data_registros'] = VozClienteFormulario::withCount('numeroPreguntas')->where('activo', true);
        if ($request->search) {
            $formulario['data_registros'] = $formulario['data_registros']->where('nombre', 'like', '%' . $request->search . '%');
        }
        if ($inicio >= 0) {
            $formulario['data_registros'] = $formulario['data_registros']->limit($request->rowsPerPage)
                ->offset($inicio)->orderBy('created_at', 'DESC');
        }

        $formulario['data_registros'] = $formulario['data_registros']->get();

        return $formulario;
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION
        $dataform = $request->dataform;
        $nombreform = $request->nombreform;


        DB::beginTransaction();
        try {

            $formulario = new VozClienteFormulario();
            $formulario->nombre = $nombreform;
            $formulario->user_id = $usuario_id;
            $formulario->save();


            foreach ($dataform as $key => $pregunta) {
                $formularioPregunta = new VozClienteFormularioPregunta();
                $formularioPregunta->voz_cliente_pregunta_id = $pregunta['id'];
                $formularioPregunta->voz_cliente_formulario_id = $formulario->id;
                $formularioPregunta->save();
            }


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];
            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formulario = VozClienteFormulario::with('preguntasAcivas')->where('id', $id)->first();

        return $formulario;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION
        $dataform = $request->dataform;


        DB::beginTransaction();
        try {


            $deleteRelacion = VozClienteFormularioPregunta::where('voz_cliente_formulario_id', $id)->delete();


            foreach ($dataform as $key => $pregunta) {
                $formularioPregunta = new VozClienteFormularioPregunta();
                $formularioPregunta->voz_cliente_pregunta_id = $pregunta['pregunta']['id'];
                $formularioPregunta->voz_cliente_formulario_id = $id;
                $formularioPregunta->save();
            }


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];
            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $deleteForm = VozClienteFormulario::find($id);
            $deleteForm->activo = 0;
            $deleteForm->update();

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'El estado del formulario ahora es inactivo.',
            ];
            return $data_return;
        } catch (Exception $e) {
            // dd($e);
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Error al cambiar de estado.',
            ];
            return $data_return;
        }
    }
}
