<?php

namespace App\Http\Controllers;

use App\Models\UsuarioAppMaster;
use Illuminate\Http\Request;
use Session;

class UsuarioController extends Controller
{
    public function validarAppmaster(Request $request)
    {
        $consulta_usuario = UsuarioAppMaster::where('id_grupo', config('app.modulo'))
        ->where('id_usuario', $request->idusuario)
        ->first();
        
        // dd($consulta_usuario);    


        if (is_null($consulta_usuario)) {
            return redirect('/404');
        } else {
            Session::put('id_usuario_calidad_cos', $consulta_usuario->id_usuario);
            Session::put('usuario_calidad_cos', $consulta_usuario->usuario);
            Session::put('nom_completo_calidad_cos', $consulta_usuario->nombre);
            Session::put('numero_rol_calidad_cos', $consulta_usuario->numero_rol);

            if ($consulta_usuario->numero_rol == 1) {
                return redirect('/admin');
            }
            if ($consulta_usuario->numero_rol == 2) {
                return redirect('/asesor');
            }
            if ($consulta_usuario->numero_rol == 3) {
                return redirect('/control');
            }
            if ($consulta_usuario->numero_rol == 4) {
                return redirect('/auditor');
            }
            if ($consulta_usuario->numero_rol == 5) {
                return redirect('/supervisor');
            }
            // if ($consulta_usuario->numero_rol == 6) {
            //     return redirect('/calidad');
            // }
        }
    }

}
