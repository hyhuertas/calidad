<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Models\Excel\ExporteReporte;
use App\Models\Matriz;

class InformeController extends Controller
{
    public function matrizActiva(Request $request, $id)
    {
        $matriz = Matriz::where('id', $id)->select('activo')->first();
        if ($matriz['activo'] == 1) {
            return $matriz;
        }else {
            return json_encode('matriz desactivada');
        }

    }

    public function descargarExcel(Request $request)
    {

        //dd($request);
        if ($request->form['tipoReporte']==1) {
            $validaciones = $request->validate([
                'form.tipoReporte' => 'required',
                'form.fechaInicial' => 'required',
                'form.fechaFinal' => 'required',
            ]);
        }
        $fechaInicial=$request->form['fechaInicial'];
        $fechaFinal=$request->form['fechaFinal'];
        $id=$request->form['tipoReporte'];
        $id_matriz = $request->idm;
        $clave = $request->clave;

        $datostruncados = Matriz::where('id', $id_matriz)->select('activo')->first();
        
        $fecha=date('Y-m-d');
        $reporte=new ExporteReporte($fechaInicial, $fechaFinal, $id, $id_matriz, $clave);
        //dd($reporte);
         return Excel::download($reporte, 'Reporte_general_'.$fecha.'.xlsx');
        
    }
}
