<?php

namespace App\Http\Controllers;

use App\Models\ClasificacionItem;
use Illuminate\Http\Request;

class ClasificacionItemController extends Controller
{
    public function listarItems(Request $request)
    {
        $items = ClasificacionItem::where('activo', true)
        ->where('nivel', $request->nivel)
        ->where('padre_id', $request->padre_id)
        ->where('clasificacion_id', $request->clasificacion_id)
        ->orderBy('nombre', 'ASC')
        ->get();

        return $items;
    }
}
