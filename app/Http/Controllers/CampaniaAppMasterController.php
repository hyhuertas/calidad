<?php

namespace App\Http\Controllers;

use App\Models\CampaniaAppMaster;
use App\Models\Grupo;
use App\Models\GrupoUsuario;
use App\Models\MatrizAuditor;
use App\Models\MiosCampaign;
use App\Models\UsuarioAppMaster;
use Illuminate\Http\Request;
use DB;
use Session;

class CampaniaAppMasterController extends Controller
{
    public function listarCampania(Request $request)
    {
        $items['master'] = CampaniaAppMaster::where('estadoModulo', 'Habilitada')
            ->where('tipoModulo', 'Campaña')
            ->select('descripcion_modulo AS name', 'id_modulo AS id')
            ->orderBy('descripcion_modulo', 'ASC')
            ->get();

        $items['mios'] = MiosCampaign::where('is_active', true)
            ->select('name', 'id')
            ->orderBy('name', 'ASC')->get();

        return $items;
    }

    public function buscarAsesor(Request $request)
    {
        $rolCalidad = Session::get('numero_rol_calidad_cos');

        $matriz_id = $request->matriz_id;
        $idsGrupo = [];
        // $asesor = [];
        $validarAsesorGrupo = null;
        $gruposMatriz = Grupo::where('matriz_id', $matriz_id)
            ->select('id')->get();

        $idsGrupo = $gruposMatriz->pluck('id');

        $asesorAppmaster = UsuarioAppMaster::where('id_grupo', config('app.modulo'))
            ->where('numero_rol', 2)
            ->where('cedula_usuario', $request->cedula_asesor)
            ->select('id_usuario', 'nombre', 'cedula_usuario')
            ->first();

        if ($rolCalidad == 4) {
            if ($asesorAppmaster) {
                return $asesorAppmaster;
            }
            return [];
        }
        //  dd($asesor->id_usuario);
        if (count($idsGrupo) > 0 && $asesorAppmaster) {
            $id_usuarioAppmaster = $asesorAppmaster->id_usuario;
            // return $id_usuarioAppmaster;
            $validarAsesorGrupo = GrupoUsuario::whereIn('grupo_id', $idsGrupo)
                ->where('usuario_id', $id_usuarioAppmaster)
                ->count();
        }
        if ($validarAsesorGrupo > 0) {
            return  $asesorAppmaster;
        }
        // dd($validarAsesorGrupo);


        return [];
    }

    public function listarAuditores()
    {
        $items = UsuarioAppMaster::where('id_grupo', config('app.modulo'))
            ->where('numero_rol', 4)
            ->select('id_usuario', 'nombre', 'cedula_usuario')
            ->get();

        return $items;
    }

    public function guardarAuditores(Request $request)
    {
        $auditores = $request->dataForm;
        $idMatriz = $request->idMatriz;



        DB::beginTransaction();
        try {

            $deleteAuditores = MatrizAuditor::where('matriz_id', $idMatriz)
                ->delete();

            foreach ($auditores as $key => $auditorid) {
                $crearAuditor = MatrizAuditor::create([
                    'matriz_id' => $idMatriz,
                    'usuario_id' => $auditorid
                ]);
            }


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }
}
