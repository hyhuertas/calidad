<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grupo;
use App\Models\AsesorGrupo;
use App\Models\GrupoUsuario;
use App\Models\UsuarioAppMaster;
use App\Models\Supervisor;
use Session;

class GrupoController extends Controller
{
    public function countGruposSupervisor(Request $request)
    {

        $matrizId = $request->matrizId;
        $supervisorId = $request->supervisorId;
        $nombreCampania = $request->nombreCampania ? str_replace(" ", "-", mb_strtoupper($request->nombreCampania,'utf-8')) : '';
        $supervisorNombre = str_replace(" ", "-", mb_strtoupper($request->supervisorNombre,'utf-8'));


        $countGrupos = Grupo::where('supervisor_id', $supervisorId)
            ->where('matriz_id', $matrizId)
            ->count();
        $nombre = ($nombreCampania ? ($nombreCampania ) : 'NO-APLICA') . "_" . $supervisorNombre . "_" . ($countGrupos + 1);
        
        return [
            "nombre" => $nombre
        ];
    }
    public function crearGrupo(Request $request)
    {
        $id_usuario = Session::get('id_usuario_calidad_cos');
        $crm = Session::get('usuario_calidad_cos');

        $validaDatos = $request->validate([
            'id' => 'required|numeric',
            'nombre' => 'required',
            'correo_jefe_opraciones' => 'required',
            'correo_supervisor' => 'required',
            'supervisor_id' => 'required|numeric'
            //'primerNombre' => 'required|min:3|max:20|regex:/^[A-Za-z]*$/',
        ]);

        $consulta_usuario = UsuarioAppMaster::where('id_usuario', $request->supervisor_id)
            ->first();

        $consulta_supervisor = Grupo::where('matriz_id', $request->id)->where('supervisor_id', $request->supervisor_id)->exists();

        if ($consulta_supervisor == true) {
            return json_encode('Este supervisor ya tiene un grupo creado en esta matriz');
        }

        $grupo = new Grupo();
        $grupo->nombre = $request->nombre;
        $grupo->matriz_id = $request->id;
        $grupo->supervisor_id = $request->supervisor_id;
        $grupo->correo_jefe_opraciones = $request->correo_jefe_opraciones;
        $grupo->correo_supervisor = $request->correo_supervisor;
        $grupo->usuario_creador_id = $id_usuario;
        $grupo->save();

        return $grupo;
    }

    public function eliminargrupo(Request $request)
    {

        $grupo = Grupo::where('id', $request->idGrupoEliminar);
        $asesoresGrupo = GrupoUsuario::where('grupo_id', $request->idGrupoEliminar);
        $asesoresGrupo->delete();
        $grupo->delete();

        return json_encode('El grupo y sus asesores han sido eliminados de esta matriz');
    }

    public function grupos($id)
    {
        $grupos = Grupo::where('matriz_id', $id)->get();
        return $grupos;
    }

    public function supervisores()
    {
        $crm = Session::get('usuario_calidad_cos');

        $supervisores = Supervisor::with('usuario')->where('id_modulo', config('app.modulo'))
            ->where('numero_rol', 5)->get();
        foreach ($supervisores as $key => $value) {
            $supervisor[$key]['id_usuario'] = $value->id_usuario;
            $supervisor[$key]['supervisornombre'] = $value['usuario']->nombre_usuario . ' ' . $value['usuario']->apellido_usuario;
        }

        return $supervisor;
    }

    public function busquedaAsesor(Request $request)
    {
        $validaDatos = $request->validate([
            'cedula' => 'required|numeric|digits_between:5,11|regex:/[0-9]/',
            //'primerNombre' => 'required|min:8|max:11|regex:/^[A-Za-z]*$/',
        ]);

        //$asesor = AsesorGrupo::where('cedula_usuario', $request->cedula)->get();

        $asesor = UsuarioAppMaster::where('id_grupo', config('app.modulo'))
            ->where('numero_rol', 2)
            ->where('cedula_usuario', $request->cedula)
            ->select('id_usuario', 'nombre', 'cedula_usuario')->first();
        return $asesor;
        //dd($request);
    }

    public function agregarAsesor(Request $request)
    {
        $validaDatos = $request->validate([
            'idGrupo' => 'required',
            'idAsesor' => 'required',
            'idMatriz' => 'required',
        ]);

        //$grupos = Grupo::with('matriz')->where('matriz_id',$request->idMatriz)->pluck('id');

        //consulta para traer los id de los grupos de una misma matriz
        $grupos = Grupo::with('matriz')->where('matriz_id', $request->idMatriz)->select('id')->get();

        $ids =  $grupos->pluck('id');

        $existenciagrupo = GrupoUsuario::whereIn('grupo_id', $ids)->where('usuario_id', $request->idAsesor)->exists();

        //dd($existenciagrupo);

        //valida si el asesor existe creado en el mismo grupo
        $existencia = GrupoUsuario::where('grupo_id', $request->idGrupo)->where('usuario_id', $request->idAsesor)->exists();
        if ($existenciagrupo === true) {
            return json_encode('usuario ya registrado en un grupo de esta matriz, y en esta matriz');
        } else {
            $asesor_agregado = new GrupoUsuario();
            $asesor_agregado->grupo_id = $request->idGrupo;
            $asesor_agregado->usuario_id = $request->idAsesor;
            $asesor_agregado->save();
        }

        return $asesor_agregado;
    }

    public function eliminarAsesor(Request $request)
    {
        $validaDatos = $request->validate([
            'idGrupo' => 'required',
            'idAsesor' => 'required',
        ]);
        $asesorEliminar = GrupoUsuario::where('grupo_id', $request->idGrupo)->where('usuario_id', $request->idAsesor);
        $asesorEliminar->delete();

        return json_encode('asesor eliminado del grupo');
    }

    public function misAsesores(Request $request)
    {
        $validaDatos = $request->validate([
            'idGrupoPersonal' => 'required'
        ]);

        $miGrupoAsesores = GrupoUsuario::with('asesores')->where('grupo_id', $request->idGrupoPersonal)->get();

        return $miGrupoAsesores;
        //dd($misAsesores);
    }
}
