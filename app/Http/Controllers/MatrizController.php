<?php

namespace App\Http\Controllers;

use App\Models\Matriz;
use App\Models\MatrizAuditoria;
use Illuminate\Http\Request;
use DB;
use Session;

class MatrizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function gestionesMatriz(Request $request)
    {
        $matriz_id = $request->matriz_id;
        $fechaInicial = $request->form['fechaInicial'];
        $fechaFinal = $request->form['fechaFinal'];

        $inicio = -1;
        if ($request->rowsPerPage && $request->page) {
            $inicio = ($request->page - 1) * $request->rowsPerPage;
        };

        $matrices['total_registros'] = MatrizAuditoria::where('matriz_id', $matriz_id)
            ->whereDate('created_at', '>=', $fechaInicial)
            ->whereDate('created_at', '<=', $fechaFinal)
            ->count();

        $matrices['data_registros'] =  MatrizAuditoria::with('detalleMatriz', 'detalleAuditor')->where('matriz_id', $matriz_id)
            ->whereDate('created_at', '>=', $fechaInicial)
            ->whereDate('created_at', '<=', $fechaFinal);
        if ($inicio >= 0) {
            $matrices['data_registros'] = $matrices['data_registros']->limit($request->rowsPerPage)
                ->offset($inicio)->orderBy('created_at', 'DESC');
        }
        $matrices['data_registros'] = $matrices['data_registros']->get();



        return $matrices;
    }
    public function index(Request $request)
    {
         
        // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');

        $rolCalidad = Session::get('numero_rol_calidad_cos');
        $usuario_id = Session::get('id_usuario_calidad_cos');

        $inicio = -1;
        if ($request->rowsPerPage && $request->page) {
            $inicio = ($request->page - 1) * $request->rowsPerPage;
        };
        /*
        User::whereHas('comments', function($q) use ($threadId) {
  $q->whereHas('post', function($q) use ($threadId) {
    $q->where('thread_id', $threadId);
  });
})->get();
        */

        $matrices['total_registros'] = Matriz::with('tipoMatriz', 'auditores', 'supervisores');

        if ($rolCalidad == 4) {
            //AUDITOR
            $matrices['total_registros'] = $matrices['total_registros']->where('activo', 1)
                ->whereHas('auditores', function ($q) use ($usuario_id) {
                    $q->where('usuario_id', $usuario_id);
                });
        }
        if ($rolCalidad == 5) {
            //AUDITOR
            $matrices['total_registros'] = $matrices['total_registros']->where('activo', 1)
                ->whereHas('supervisores', function ($q) use ($usuario_id) {
                    $q->where('supervisor_id', $usuario_id);
                });
        }

        if ($request->search) {
            $matrices['total_registros'] = Matriz::where('nombre', 'like', '%' . $request->search . '%');
        }
        $matrices['total_registros'] = $matrices['total_registros']->count();


        $matrices['data_registros'] =  Matriz::with('tipoMatriz', 'auditores', 'supervisores');
        if ($rolCalidad == 4) {
            //AUDITOR
            $matrices['data_registros'] = $matrices['data_registros']->where('activo', 1)
                ->whereHas('auditores', function ($q) use ($usuario_id) {
                    $q->where('usuario_id', $usuario_id);
                });
        }
        if ($rolCalidad == 5) {
            //AUDITOR
            $matrices['data_registros'] = $matrices['data_registros']->where('activo', 1)
                ->whereHas('supervisores', function ($q) use ($usuario_id) {
                    $q->where('supervisor_id', $usuario_id);
                });
        }


        if ($request->search) {
            $matrices['data_registros'] = $matrices['data_registros']->where('nombre', 'like', '%' . $request->search . '%');
        }

        if ($inicio >= 0) {
            $matrices['data_registros'] = $matrices['data_registros']->limit($request->rowsPerPage)
                ->offset($inicio)->orderBy('created_at', 'DESC');
        }

        $matrices['data_registros'] = $matrices['data_registros']->get();



        return $matrices;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // 
        // Session::get('numero_rol_calidad_cos');
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;

        $nombre = $formdata['nombre'];
        $tipo_matriz_id = $formdata['tipo_matriz_id'];
        $rango_id = $formdata['rango_id'];
        $tipo_peso_id = $formdata['tipo_peso_id'];
        $campania_asociada = $formdata['campania_asociada'];
        $aplicar_encuesta = $formdata['aplicar_encuesta'];
        $crm_id = $formdata['crm_id'];
        $grupo = $formdata['grupo'];
        $encuesta_id = $formdata['encuesta_id'];


        $rules = [
            'dataform.nombre' => 'required|unique:matrices,nombre|min:3|max:150',
            'dataform.tipo_matriz_id' => 'required',
            'dataform.campania_asociada' => 'required',
            'dataform.aplicar_encuesta' => 'required',
        ];

        if ($tipo_matriz_id == 2) {
            $validValor =  ['dataform.rango_id' => 'required', 'dataform.tipo_peso_id' => 'required'];
            $rules =   array_merge($rules, $validValor);
        }
        if ($campania_asociada) {
            $validCampania =  ['dataform.crm_id' => 'required'];
            $rules =   array_merge($rules, $validCampania);
        }
        if ($aplicar_encuesta) {
            $validEncuesta =  ['dataform.encuesta_id' => 'required'];
            $rules =   array_merge($rules, $validEncuesta);
        }


        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe una matriz con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.',
            'dataform.tipo_matriz_id.required' => 'El campo TIPO DE MATRIZ es requerido.',
            'dataform.rango_id.required' => 'El campo RANGO DE PESO es requerido.',
            'dataform.tipo_peso_id.required' => 'El campo ¿PESO EN MÓDULO O EN ITEMS? es requerido.',
            'dataform.campania_asociada.required' => 'El campo CAMPAÑA ASOCIADA CRM es requerido.',
            'dataform.crm_id.required' => 'El campo SELECCIONAR CRM es requerido.',
            'dataform.aplicar_encuesta.required' => 'El campo APLICAR ENCUESTA CRM es requerido.',
            'dataform.encuesta_id.required' => 'El campo SELECCIONAR ENCUESTA es requerido.',

        ];


        $this->validate($request, $rules, $messages);




        DB::beginTransaction();
        try {

            $insertMatriz = new Matriz();

            $insertMatriz->nombre = $nombre;
            $insertMatriz->tipo_matriz_id = $tipo_matriz_id;
            $insertMatriz->asociacion_crm = $campania_asociada;
            $insertMatriz->crm_id = $crm_id;
            $insertMatriz->grupo = $grupo;
            $insertMatriz->rango_total_id = $rango_id;
            $insertMatriz->tipo_peso_id = $tipo_peso_id;
            $insertMatriz->usuario_creador_id = $usuario_id;
            $insertMatriz->usuario_actualizacion_id = $usuario_id;
            $insertMatriz->tiene_encuesta = $aplicar_encuesta;
            $insertMatriz->encuesta_id = $encuesta_id;

            $insertMatriz->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $matriz = Matriz::with(
            'tipoMatriz',
            'auditores',
            'modulos.itemsModulo.subItems',
            'formuarioEncuesta.preguntasAcivas.pregunta.opcionesRespuestaFormPreguntaActivos',
            'formuarioEncuesta.preguntasAcivas.pregunta.rango'
        )->where('id', $id)->first();

        $items = [];

        if ($matriz) {
            $items = [
                "acumulado" => 0,
                "seleccion_campaign" => $matriz->seleccionCampaign,
                "formuarioEncuesta" => $matriz->formuarioEncuesta,
                "id" => $matriz->id,
                "valor_peso_validacion" => $matriz->valor_peso_validacion,
                "nombre" => $matriz->nombre,
                "tipo_matriz_id" => $matriz->tipo_matriz_id,
                "asociacion_crm" => $matriz->asociacion_crm,
                "crm_id" => $matriz->crm_id,
                "rango_total_id" => $matriz->rango_total_id,
                "tipo_peso_id" => $matriz->tipo_peso_id,
                "usuario_creador_id" => $matriz->usuario_creador_id,
                "usuario_actualizacion_id" => $matriz->usuario_actualizacion_id,
                "tiene_encuesta" => $matriz->tiene_encuesta,
                "encuesta_id" => $matriz->encuesta_id,
                "activo" => $matriz->activo,
                "created_at" => date_format($matriz->created_at, "Y-m-d H:i:s"),
                "updated_at" => date_format($matriz->updated_at, "Y-m-d H:i:s"),
                "tipo_matriz" => $matriz->tipoMatriz,
                "auditores" => $matriz->auditores->pluck('usuario_id'),
                "modulos" => [],
            ];
            //recorrido de los modulos
            foreach ($matriz->modulos as $keyModulo => $modulo) {

                if (isset($items['acumulado']) && isset($modulo->peso) && $modulo->activo) {
                    $items['acumulado'] = number_format((float) $items['acumulado'] + (float) $modulo->peso, 1);
                }


                $items['modulos'][$keyModulo] = [
                    "id" => $modulo->id,
                    "idConsecutivoModulo" => $keyModulo,
                    "nombre" => $modulo->nombre,
                    "peso" => $modulo->peso,
                    "premium" => $modulo->premium,
                    "sigla" => $modulo->sigla,
                    "nivel" => $modulo->nivel,
                    "padre_id" => $modulo->padre_id,
                    "activo" => $modulo->activo,
                    "usuario_creador_id" => $modulo->usuario_creador_id,
                    "usuario_actualizacion_id" => $modulo->usuario_actualizacion_id,
                    "matriz_id" => $modulo->matriz_id,
                    "error_critico" => $modulo->error_critico,
                    "items_modulo" => [],
                    // "items_modulo_activos" => true,

                ];
                //recorrido de los items
                foreach ($modulo->itemsModulo as $keyItem => $item) {

                    if (isset($items['acumulado']) && isset($item->peso) && $item->activo) {
                        $items['acumulado'] = number_format((float) $items['acumulado'] + (float) $item->peso, 1);
                    }

                    $items['modulos'][$keyModulo]['items_modulo'][$keyItem] = [
                        "id" => $item->id,
                        "idConsecutivoModulo" => $keyModulo,
                        "idConsecutivoModuloItem" => $keyItem,
                        "nombre" => $item->nombre,
                        "peso" => $item->peso,
                        // "error_critico_padre" => $items['modulos'][$keyModulo]['error_critico'],
                        "peso_padre" => $items['modulos'][$keyModulo]['peso'],
                        "total_items" => count($modulo->itemsModulo),
                        "premium" => $item->premium,
                        "sigla" => $item->sigla,
                        "nivel" => $item->nivel,
                        "padre_id" => $item->padre_id,
                        "activo" => $item->activo,
                        "usuario_creador_id" => $item->usuario_creador_id,
                        "usuario_actualizacion_id" => $item->usuario_actualizacion_id,
                        "matriz_id" => $item->matriz_id,
                        "error_critico" => $item->error_critico,
                        "type_select" => $item->type_select,
                        "subItems" => $item->subItems,
                        "subItemsActivos" => true,

                    ];
                    // $items['modulos'][$key]['items'][] = 1;
                }
            }
        }


        return $items;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matriz =  Matriz::find($id);
        $estadoMatriz = $matriz->activo;

        $matriz->activo = !$estadoMatriz;
        $matriz->save();
        dd($matriz->activo);
    }

    public function nombrematriz(Request $request)
    {
        dd($request->headers);
    }
}
