<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MatrizAuditoria;
use Session;
use DateTime;

class AsesorController extends Controller
{
    public function auditorias()
    {
        $id_usuario = Session::get('id_usuario_calidad_cos');
        $auditoriaspropias = MatrizAuditoria::with('detalleMatriz')->with('detalleAuditor')->with('itemsAfectados')->where('asesor_id', $id_usuario)
        ->orderBy('created_at', 'DESC')
        ->get();
        $auditoriaPropia = [];
        if ($auditoriaspropias!=[]) {
            foreach ($auditoriaspropias as $key => $auditoria) {
                $auditoriaPropia[$key]['id'] = $auditoria->id;
                $auditoriaPropia[$key]['matriz'] = $auditoria->detalleMatriz->nombre;
                $auditoriaPropia[$key]['IdMatriz'] = $auditoria->matriz_id;
                $auditoriaPropia[$key]['cedula_asesor'] = $auditoria->cedula_asesor;
                $auditoriaPropia[$key]['fecha_auditoria'] = $auditoria->created_at->format('Y-m-d');
                $auditoriaPropia[$key]['auditado_por'] = $auditoria->detalleAuditor->nombre;
                $auditoriaPropia[$key]['telefono_auditado'] = $auditoria->tlf_auditados;
                $auditoriaPropia[$key]['calificacion'] = $auditoria->total_punaje;
                $auditoriaPropia[$key]['fecha_llamada_auditoria'] = $auditoria->fecha_llamada;
                $auditoriaPropia[$key]['retroalimentacion_auditor'] = $auditoria->retroalimentacion_auditor;
                $auditoriaPropia[$key]['compromiso_calidad'] = $auditoria->compromiso_calidad;
                $auditoriaPropia[$key]['retroalimentacion_supervisor'] = $auditoria->retroalimentacion_supervisor;
                $auditoriaPropia[$key]['observacion_auditor'] = $auditoria->observacion_auditor;
                $auditoriaPropia[$key]['compromiso'] = $auditoria->compromiso;
                if (is_null($auditoria->auditor_retroalimentacion_id) && is_null($auditoria->supervisor_retroalimentacion_id)) {
                    $auditoriaPropia[$key]['retroalimentada'] = 'NO';
                } else {
                    $auditoriaPropia[$key]['retroalimentada'] = 'SI';
                }
                $campanas = $auditoria->detalleMatriz->campana;
                
    
                $crm = $auditoria->detalleMatriz->crm_id;
                $auditoriaPropia[$key]['campana'] = $auditoria->detalleMatriz->nombre_campania . ($auditoria->detalleMatriz->crm_id ? ' (' . $auditoria->detalleMatriz->grupo . ')' : '');
                $auditoriaPropia[$key]['grupo'] = $auditoria->detalleMatriz->grupo;
                
            }
        }
        
        return $auditoriaPropia;
    }

    public function auditoriasAsesor()
    {
        $id_usuario = Session::get('id_usuario_calidad_cos');
        $h = new DateTime();
        $hoy = $h->format('d');
        $m = new DateTime();
        $mes = $m->format('m');
        $mesprimero = $mes-1;
        $messegundo = $mes+1;
        
        $auditoriasDia = MatrizAuditoria::where('asesor_id', $id_usuario)->whereMonth('created_at', $mes)->whereDay('created_at', $hoy)->get();
        $auditoriasMes = MatrizAuditoria::where('asesor_id', $id_usuario)->whereMonth('created_at', $mes)->get();
        $auditoriasTrimestral = MatrizAuditoria::where('asesor_id', $id_usuario)->whereMonth('created_at', '>=', $mesprimero)->whereMonth('created_at', '<=', $messegundo)->get();
                
        $a = count($auditoriasDia);
        $b = count($auditoriasMes);
        $c = count($auditoriasTrimestral);

        //promedio del dia
        $acumuladodia = 0;
        $promediodia = 0;

        foreach ($auditoriasDia as $key => $dia) {
            $acumuladodia+= $dia->total_punaje;  
        }   

        $promediodia = $acumuladodia / $a;

        //promedio del mes
        $acumuladomes = 0;
        $promediomes = 0;

        foreach ($auditoriasMes as $key => $mes) {
            $acumuladomes+= $mes->total_punaje;  
        }
        
        $promediomes = $acumuladomes/$b;

        //promedio trimestre             
        $acumuladotrimestre = 0;
        $promediotrimestre = 0;

        foreach ($auditoriasTrimestral as $key => $trimestre) {
            $acumuladotrimestre+= $trimestre->total_punaje;  
        }   
        
        $promediotrimestre = $acumuladotrimestre/$c;

        return ['dia' => $a,'mes' => $b,'trimestre' => $c, 'promedio_dia' => round($promediodia), 'promedio_mes' => round($promediomes), 'promedio_trimestre' => round($promediotrimestre)];
    }

    public function compromiso(Request $request)
    {
        $auditoria_id = $request->id;
        $validaciones = $request->validate([
            'id' => 'required|numeric',
            // 'compromiso' => 'required'
        ]);

        $hoy = date('Y-m-d H:s:i');

        $guardarCompromiso = MatrizAuditoria::where('id',$auditoria_id)->first();
        $guardarCompromiso->compromiso = $request->compromiso;
        $guardarCompromiso->compromiso_calidad = $request->compromiso_calidad;
        $guardarCompromiso->fecha_compromiso = $hoy;
        $guardarCompromiso->save();

        return $guardarCompromiso;
    }

    public function asesorRetroalimentar(Request $request)
    {
        $dataForm = $request->dataForm;

        $auditoria = MatrizAuditoria::with('detalleAsesor')->with('detalleAuditor')->with('detalleMatriz');
            $id_auditoria = $request->id_auditoria;

            $auditoria = $auditoria->where('id', $id_auditoria)
                ->with(['itemsAfectados.items.subitems' => function ($query) use ($id_auditoria) {
                    $query->where('matriz_auditoria_id', $id_auditoria);
                }, 'itemsAfectados.items' => function ($query) use ($id_auditoria) {
                    $query->where('matriz_auditoria_id', $id_auditoria);
                }]);
            // ->with(['itemsAfectados.items' => function ($query) use ($id_auditoria){
            //     $query->where('matriz_auditoria_id', $id_auditoria);
            // }])->whereHas('itemsAfectados', function ($query) use ($id_auditoria) {
            //     $query->whereHas('items', function ($query) use ($id_auditoria) {
            //         $query->where('matriz_auditoria_id', $id_auditoria);
            //     });
            // });

        $auditoria = $auditoria->where('cedula_asesor', $dataForm['cedula_asesor'])
            ->where('matriz_id', $dataForm['IdMatriz']);

        $auditoria = $auditoria->first();
       

        return $auditoria;
    }

}
