<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\GrupoUsuario;
use App\Models\Matriz;
use App\Models\MatrizAuditoria;
use App\Models\MatrizAuditoriaItem;
use App\Models\VozClienteRespuesta;
use Illuminate\Http\Request;
use DB;
use Session;

class AuditoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');
        $rolCalidad = Session::get('numero_rol_calidad_cos');
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formAsesor = $request->formAsesor;
        $seleccionItemsAfectacion = $request->seleccionItemsAfectacion;
        $matriz_id = $request->matriz_id;
        $encuesta = $request->encuesta;
         /* dd(
             $formAsesor,
             $seleccionItemsAfectacion,
             $matriz_id
         ); */



        DB::beginTransaction();
        try {
            unset($formAsesor['nombre_asesor']);
            // $formAsesor['total_punaje'] = 60;
            $formAsesor['maximo_puntaje'] = 100;
            $formAsesor['matriz_id'] = $matriz_id;
            $formAsesor['auditor_id'] = $usuario_id;
            $formAsesor['rol_auditor'] = $rolCalidad;
            $formAsesor['seleccionar_dia'] = $formAsesor['seleccionar_dia'] ? $formAsesor['seleccionar_dia'] : 0;
            if ($formAsesor['total_punaje'] == null) {
                $formAsesor['total_punaje'] = 0;
            }
            $insertAuditoriaId =  MatrizAuditoria::create($formAsesor);


            foreach ($seleccionItemsAfectacion as $key => $modulo) {
                $moduloAuditoriaId = MatrizAuditoriaItem::create([
                    'matriz_auditoria_id' => $insertAuditoriaId->id,
                    'modulo_id' => $modulo['padre_id'],
                    'nivel' => 1,
                    'padre_id' => null,
                    'cumple' => false,
                    'no_cumple' => false,
                    'no_aplica' => false,
                    'si' => false,
                    'no' => false,
                    'tipo_matriz_id' => $modulo['tipo_matriz_id'],
                    'puntaje' => ($modulo['puntaje']  != 'NaN') ? $modulo['puntaje'] : null,
                    'puntaje_obtenido' => ($modulo['puntaje_obtenido'] != 'NaN') ? $modulo['puntaje_obtenido'] : null,
                ]);
                foreach ($modulo['itemsAfectados'] as $key => $item) {
                    // dd($item['type_seleccion'] == 'cumple' ? true : false);
                    $itemAuditoriaId = null;
                    $itemAuditoriaId = MatrizAuditoriaItem::create([
                        'matriz_auditoria_id' => $insertAuditoriaId->id,
                        'modulo_id' => $item['item_id'],
                        'nivel' => $item['typeItem'] == 'item' ? 2 : 3,
                        'padre_id' => $item['padreitem'],
                        'cumple' => ($item['type_seleccion'] == 'cumple' ? true : false),
                        'no_cumple' => $item['type_seleccion'] == 'no_cumple' ? true : false,
                        'no_aplica' => $item['type_seleccion'] == 'no_aplica' ? true : false,
                        'si' => $item['type_seleccion'] == 'si' ? true : false,
                        'no' => $item['type_seleccion'] == 'no' ? true : false,
                        'tipo_matriz_id' => $modulo['tipo_matriz_id'],
                        'puntaje' => ($item['puntaje']  != 'NaN') ? $item['puntaje'] : null,
                        'puntaje_obtenido' => ($item['puntaje_obtenido']  != 'NaN') ? $item['puntaje_obtenido'] : null,
                    ]);
                }
            }


            if (isset($encuesta)) {

                $voz_cliente_formulario_id = $encuesta['id'];
                $auditoria_id = $insertAuditoriaId->id;
                $user_id = $usuario_id;
                //id, auditoria_id, user_id, voz_cliente_pregunta_id, voz_cliente_formulario_id, respuesta, respuesta_id, created_at, updated_at
                foreach ($encuesta['preguntas_acivas'] as $key => $pregunta) {
                    $voz_cliente_pregunta_id = $pregunta['pregunta']['id'];
                    $respuesta = $pregunta['pregunta']['respuesta_usuario'];
                    $respuesta_usuario_otra = $pregunta['pregunta']['respuesta_usuario_otra'];
                    $respuesta_usuario_otra_enable = $pregunta['pregunta']['respuesta_usuario_otra_enable'];
                    $tipo_pregunta_id = $pregunta['pregunta']['tipo_pregunta_id'];

                    $insertPreguta = new VozClienteRespuesta();
                    $insertPreguta->auditoria_id = $auditoria_id;
                    $insertPreguta->user_id = $user_id;
                    $insertPreguta->voz_cliente_pregunta_id = $voz_cliente_pregunta_id;
                    $insertPreguta->voz_cliente_formulario_id = $voz_cliente_formulario_id;

                    if ($tipo_pregunta_id == 2) {
                        if ($respuesta_usuario_otra && $respuesta_usuario_otra_enable) {
                            $insertPreguta->respuesta = $respuesta_usuario_otra;
                        }
                        $insertPreguta->respuesta_id = $respuesta;
                    } else {
                        $insertPreguta->respuesta = $respuesta;
                    }

                    $insertPreguta->save();
                }
            }


            // dd($insertAuditoriaId, $seleccionItemsAfectacion);

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];
            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }




    public function detalleGruposCalidad(Request $request)
    {
        $grupo_id = $request->id_grupo;
        $matriz_id = $request->matriz_id;
        $fecha_inicial = $request->fecha_inicial;
        $fecha_final = $request->fecha_final;


        $grupo = GrupoUsuario::with('detalleAsesor')
            ->where('grupo_id', $grupo_id)
            ->get();

        $retroalimentacion = MatrizAuditoria::with('detalleAsesor')->where('matriz_id',  $matriz_id)
            ->whereIn('asesor_id', $grupo->pluck('usuario_id'))
            ->whereDate('created_at', '>=', $fecha_inicial)
            ->whereDate('created_at', '<=', $fecha_final)
            ->get();



        return ($retroalimentacion);
    }


    public function buscarGruposCalidad(Request $request)
    {
        $formData = $request->formData;
        $search = $formData['search'];

        //formBuscar.search
        $item = [];
        $grupos = Grupo::with('asesoresGrupo')->where('activo', true);
        if ($search && $formData['nombreGrupo'] != '' && $formData['nombreGrupo'] != null) {
            $grupos = $grupos->where('nombre', 'LIKE', '%' . $formData['nombreGrupo'] . '%');
        }
        $grupos = $grupos->get();

        foreach ($grupos as $keyGrupo => $grupo) {
            $conRetroalimentacion = 0;
            $sinRetroalimentacion = 0;
            foreach ($grupo->asesoresGrupo as $keyAsesor => $asesor) {
                if ($formData['fechaFinal'] && $formData['fechaInicial']) {
                    $conRetroalimentacion += MatrizAuditoria::where('matriz_id', $grupo->matriz_id)
                        ->where('asesor_id', $asesor->usuario_id)
                        ->where(function ($query) {
                            $query->whereNotNull('auditor_retroalimentacion_id')
                                ->orWhereNotNull('supervisor_retroalimentacion_id');
                        })
                        ->whereDate('created_at', '>=', $formData['fechaInicial'])
                        ->whereDate('created_at', '<=', $formData['fechaFinal'])
                        ->count();

                    $sinRetroalimentacion = MatrizAuditoria::where('matriz_id', $grupo->matriz_id)
                        ->where('asesor_id', $asesor->usuario_id)
                        ->whereNull('auditor_retroalimentacion_id')
                        ->whereNull('supervisor_retroalimentacion_id')
                        ->whereDate('created_at', '>=', $formData['fechaInicial'])
                        ->whereDate('created_at', '<=', $formData['fechaFinal'])
                        ->count();
                } else {
                    $conRetroalimentacion += MatrizAuditoria::where('matriz_id', $grupo->matriz_id)
                        ->where('asesor_id', $asesor->usuario_id)
                        ->where(function ($query) {
                            $query->whereNotNull('auditor_retroalimentacion_id')
                                ->orWhereNotNull('supervisor_retroalimentacion_id');
                        })
                        ->count();

                    $sinRetroalimentacion = MatrizAuditoria::where('matriz_id', $grupo->matriz_id)
                        ->where('asesor_id', $asesor->usuario_id)
                        ->whereNull('auditor_retroalimentacion_id')
                        ->whereNull('supervisor_retroalimentacion_id')
                        ->count();
                }
            }
            $item[] = [
                "id" =>  $grupo->id,
                "nombre" =>  $grupo->nombre,
                "matriz_id" =>  $grupo->matriz_id,
                "supervisor_id" =>  $grupo->supervisor_id,
                "total_asesores" =>  count($grupo->asesoresGrupo),
                "total_conretroalimentacion" =>  $conRetroalimentacion,
                "total_sinretroalimentacion" =>  $sinRetroalimentacion,

            ];
        }

        return $item;
    }
    public function guardarRetroalimentacion(Request $request)

    {
        DB::beginTransaction();
        try {

            $rolCalidad = Session::get('numero_rol_calidad_cos');
            $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

            $auditoria = MatrizAuditoria::find($request->idAuditoria);
            if ($rolCalidad == 5) { //SUPERVISOR
                $auditoria->retroalimentacion_supervisor = $request->retroalimentacion;
                $auditoria->fecha_retroalimentacion_supervisor = date('Y-m-d H:s:i');
                $auditoria->supervisor_retroalimentacion_id = $usuario_id;
            } else if ($rolCalidad == 4) { //AUDITOR
                $auditoria->retroalimentacion_auditor = $request->retroalimentacion;
                $auditoria->fecha_retroalimentacion_auditor = date('Y-m-d H:s:i');
                $auditoria->auditor_retroalimentacion_id = $usuario_id;
            }

            // $auditoria->rol_auditor = $rolCalidad;

            $auditoria->save();

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];
            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    public function buscarAsesorAuditoria(Request $request)
    {
        $dataForm = $request->dataForm;
        $matriz_id = $request->matriz_id;

        $auditorias['auditor'] = MatrizAuditoria::with('tipoAudioria', 'detalleAsesor', 'detalleAuditor')->where('matriz_id', $matriz_id)
            ->where('asesor_id',  $dataForm['asesor_id'])
            ->where('rol_auditor',  4)
            ->whereDate('created_at', '>=', $dataForm['fechaInicial'])
            ->whereDate('created_at', '<=', $dataForm['fechaFinal'])
            ->get();
        $auditorias['supervisor'] = MatrizAuditoria::with('tipoAudioria', 'detalleAsesor', 'detalleAuditor')->where('matriz_id', $matriz_id)
            ->where('asesor_id',  $dataForm['asesor_id'])
            ->where('rol_auditor',  5)
            ->whereDate('created_at', '>=', $dataForm['fechaInicial'])
            ->whereDate('created_at', '<=', $dataForm['fechaFinal'])
            ->get();

        return $auditorias;
    }

    public function getAsesoresMatriz(Request $request)
    {
        $matriz_id = $request->matriz_id;
        $grupos = Grupo::where('matriz_id', $matriz_id)->get();
        $asesores = [];
        if (count($grupos) > 0) {
            $grupoAsesores  = GrupoUsuario::with('detalleAsesor')->whereIn('grupo_id', $grupos->pluck('id'))->get();
            return $grupoAsesores;
        }
        return $grupoAsesores;
    }


    public function buscarAsesorRetroalimentar(Request $request)
    {
        $dataForm = $request->dataForm;
        $itemsafectados = $request->itemsafectados;
        $rolCalidad = Session::get('numero_rol_calidad_cos');
        if ($rolCalidad == 5) {
            $estado_retroalimentacion = $dataForm['estadoRetroalimentacion'];
        }



        $auditoria = MatrizAuditoria::with('tipoAudioria', 'detalleAsesor', 'detalleAuditor', 'detalleMatriz');
        if ($itemsafectados) {
            $id_auditoria = $request->id_auditoria;

            $auditoria = $auditoria->where('id', $id_auditoria)
                ->with(['itemsAfectados.items.subitems' => function ($query) use ($id_auditoria) {
                    $query->where('matriz_auditoria_id', $id_auditoria);
                }, 'itemsAfectados.items' => function ($query) use ($id_auditoria) {
                    $query->where('matriz_auditoria_id', $id_auditoria);
                }]);
            // ->with(['itemsAfectados.items' => function ($query) use ($id_auditoria){
            //     $query->where('matriz_auditoria_id', $id_auditoria);
            // }])->whereHas('itemsAfectados', function ($query) use ($id_auditoria) {
            //     $query->whereHas('items', function ($query) use ($id_auditoria) {
            //         $query->where('matriz_auditoria_id', $id_auditoria);
            //     });
            // });
        }

        $auditoria = $auditoria->where('cedula_asesor', $dataForm['cedulaAesor'])
            ->where('matriz_id', $request->matriz_id)

            ->whereDate('created_at', '>=', $dataForm['fechaInicial'])
            ->whereDate('created_at', '<=', $dataForm['fechaFinal']);
        if ($rolCalidad == 5) {
            if ($estado_retroalimentacion == true) {
                $auditoria = $auditoria->where(function ($query) {
                    $query->whereNotNull('supervisor_retroalimentacion_id')
                        ->orWhereNotNull('auditor_retroalimentacion_id');
                    // $query->OrwhereNotNull('fecha_retroalimentacion_supervisor');
                });
                // $auditoria = $auditoria->whereNotNull('fecha_retroalimentacion_auditor')->whereNotNull('fecha_retroalimentacion_supervisor');
            } else {
                $auditoria = $auditoria->where(function ($query) {
                    $query->whereNull('supervisor_retroalimentacion_id')
                        ->whereNull('auditor_retroalimentacion_id');
                    // $query->WhereNull('fecha_retroalimentacion_supervisor');
                });
                // $auditoria = $auditoria->orWhereNull('fecha_retroalimentacion_auditor')->orWhereNull('fecha_retroalimentacion_supervisor');
            }
        }
        if ($itemsafectados) {
            $auditoria = $auditoria->first();
        } else {
            $auditoria = $auditoria->get();
        }


        return $auditoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
