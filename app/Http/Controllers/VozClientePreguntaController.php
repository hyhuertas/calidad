<?php

namespace App\Http\Controllers;

use App\Models\VozClientePregunta;
use App\Models\VozClientePreguntaItem;
use Illuminate\Http\Request;
use DB;
use Session;

class VozClientePreguntaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');


        $inicio = -1;
        if ($request->rowsPerPage && $request->page) {
            $inicio = ($request->page - 1) * $request->rowsPerPage;
        };

        $preguntas['total_registros'] = VozClientePregunta::where('activo', true);
        if ($request->search) {
            $preguntas['total_registros'] = VozClientePregunta::where('pregunta', 'like', '%' . $request->search . '%');
        }
        $preguntas['total_registros'] = $preguntas['total_registros']->count();


        $preguntas['data_registros'] = VozClientePregunta::with('tipoPregunta', 'opcionesRespuestaFormPreguntaActivos', 'rango')->where('activo', true);
        if ($request->search) {
            $preguntas['data_registros'] = $preguntas['data_registros']->where('pregunta', 'like', '%' . $request->search . '%');
        }
        if ($inicio >= 0) {
            $preguntas['data_registros'] = $preguntas['data_registros']->limit($request->rowsPerPage)
                ->offset($inicio)->orderBy('created_at', 'DESC');
        }

        $preguntas['data_registros'] = $preguntas['data_registros']->get();

        return $preguntas;
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION
        $dataform = $request->dataform;


        DB::beginTransaction();
        try {


            $insertPregunta = new VozClientePregunta();
            $insertPregunta->tipo_pregunta_id = $dataform['tipo_pregunta_id'];
            $insertPregunta->tipo_pregunta_item_id = $dataform['tipo_pregunta_item_id'];
            $insertPregunta->pregunta = $dataform['pregunta'];
            $insertPregunta->requerida = $dataform['requerida'];
            $insertPregunta->otra_respuesta = $dataform['otra_respuesta'];
            $insertPregunta->user_id = $usuario_id;
            $insertPregunta->save();



            if ($dataform['tipo_pregunta_id'] == 2) {
                foreach ($dataform['opcionesRespuestaFormPregunta'] as $key => $opcion) {
                    $insertPreguntaOpciones = new VozClientePreguntaItem();
                    $insertPreguntaOpciones->voz_cliente_pregunta_id = $insertPregunta->id;
                    $insertPreguntaOpciones->nombre = $opcion['nombre'];
                    $insertPreguntaOpciones->otra_respuesta = false;
                    $insertPreguntaOpciones->save();
                }

                if ($dataform['otra_respuesta']) {
                    $insertPreguntaOpciones = new VozClientePreguntaItem();
                    $insertPreguntaOpciones->voz_cliente_pregunta_id = $insertPregunta->id;
                    $insertPreguntaOpciones->nombre = 'Otra (o)';
                    $insertPreguntaOpciones->otra_respuesta = true;
                    $insertPreguntaOpciones->save();
                }
            }


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = VozClientePregunta::with('tipoPregunta.itemsTipoPregunta', 'opcionesRespuestaFormPregunta')->where('id', $id)->first();

        return $pregunta;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION
        $dataform = $request->dataform;


        DB::beginTransaction();
        try {

            /* ACTUALIZACIÓN DE PREGUNTA */
            $insertPregunta = VozClientePregunta::find($id);
            // $insertPregunta->tipo_pregunta_id = $dataform['tipo_pregunta_id'];
            $insertPregunta->tipo_pregunta_item_id = $dataform['tipo_pregunta_item_id'];
            $insertPregunta->pregunta = $dataform['pregunta'];
            $insertPregunta->requerida = $dataform['requerida'];
            $insertPregunta->otra_respuesta = $dataform['otra_respuesta'];
            $insertPregunta->user_id = $usuario_id;
            $insertPregunta->save();
            /* ACTUALIZACIÓN DE PREGUNTA */



            if ($dataform['tipo_pregunta_id'] == 2) {
                // dd($dataform['opciones_respuesta_form_pregunta']);

                foreach ($dataform['opciones_respuesta_form_pregunta'] as $key => $opcion) {
                    if (array_key_exists('id', $opcion)) {
                        $insertPreguntaOpciones = VozClientePreguntaItem::find($opcion['id']);
                        $insertPreguntaOpciones->activo = $opcion['activo'];
                    } else {
                        $insertPreguntaOpciones = new VozClientePreguntaItem();
                        $insertPreguntaOpciones->voz_cliente_pregunta_id = $id;
                        $insertPreguntaOpciones->otra_respuesta = false;
                    }
                    $insertPreguntaOpciones->nombre = $opcion['nombre'];
                    $insertPreguntaOpciones->save();
                }

                if ($dataform['otra_respuesta']) {
                    $validarOtraRespuestaActiva = VozClientePreguntaItem::where('activo', true)
                        ->where('otra_respuesta', true)
                        ->where('voz_cliente_pregunta_id', $id)->count();
                    $validarOtraRespuestaDesactivada = VozClientePreguntaItem::where('activo', false)
                        ->where('otra_respuesta', true)
                        ->where('voz_cliente_pregunta_id', $id)->first();

                    if ($validarOtraRespuestaDesactivada) {
                        //  SI LA OPCION ESTA DESACTIVADA SE ACTIVA
                        $insertPreguntaOpciones = VozClientePreguntaItem::find($validarOtraRespuestaDesactivada->id);
                        $insertPreguntaOpciones->activo = true;
                        $insertPreguntaOpciones->save();
                    } else if ($validarOtraRespuestaActiva == 0 && !$validarOtraRespuestaDesactivada) {
                        //  SI NO TIENE OPCION DE OTRA RESPUESTA CREAR LA OPCION
                        $insertPreguntaOpciones = new VozClientePreguntaItem();
                        $insertPreguntaOpciones->nombre = 'Otra (o)';
                        $insertPreguntaOpciones->otra_respuesta = true;
                        $insertPreguntaOpciones->save();
                    }
                } else {
                    $validarOtraRespuesta = VozClientePreguntaItem::where('activo', true)
                        ->where('otra_respuesta', true)
                        ->where('voz_cliente_pregunta_id', $id)->first();
                        // dd($validarOtraRespuesta );
                    if ($validarOtraRespuesta) {
                        //  SI TIENE OPCION DE OTRA SE DESCATIVA
                        $insertPreguntaOpciones = VozClientePreguntaItem::find($validarOtraRespuesta->id);
                        $insertPreguntaOpciones->activo = false;
                        $insertPreguntaOpciones->save();
                    }
                }
            }




            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información actualizada correctamente.',
            ];

            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $insertPregunta = VozClientePregunta::find($id);
            $insertPregunta->activo = 0;
            $insertPregunta->update();

                $data_return['respuesta'] = [
                    'codigo' => 202,
                    'icon' => 'mdi-check-circle',
                    'color' => 'success',
                    'text' => 'El estado del formulario ahora es inactivo.',
                ];
                return $data_return;
          
        } catch (Exception $e) {
            // dd($e);
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Error al cambiar de estado.',
            ];
            return $data_return;
        }
    }
}
