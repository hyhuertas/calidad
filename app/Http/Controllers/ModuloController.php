<?php

namespace App\Http\Controllers;

use App\Models\Modulo;
use Illuminate\Http\Request;
use DB;
USE Session;

class ModuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getExcelCol($num)
    {
        $numero = $num % 26;
        $letra = chr(65 + $numero);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getExcelCol($num2 - 1) . $letra;
        } else {
            return $letra;
        }
    }

    public function consultarSigla($idMatriz)
    {
        $countModulos = Modulo::where('nivel', 1)->where('matriz_id', $idMatriz)->count();
        return $this->getExcelCol($countModulos);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');
        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,null,null,matriz_id,' . $idMatriz . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {


            $letra = $this->consultarSigla($idMatriz);

            $modulo = new Modulo;
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            $modulo->nivel = $nivel;
            $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            $modulo->matriz_id = $idMatriz;
            $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            $validarSigla = Modulo::where('nivel', 1)
                ->where('sigla', $letra)
                ->where('matriz_id', $idMatriz)->count();
            if ($validarSigla > 0) {
                $letra = $this->consultarSigla($idMatriz);
                $modulo->sigla = $letra;
            } else {
                $modulo->sigla = $letra;
            }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }


    public function crearItems(Request $request)
    {

        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $sigla = $formdata['sigla'];
        $padre_id = $formdata['padre_id'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,null,null,padre_id,' . $padre_id . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {




            $validarCantidadItems = Modulo::where('nivel', 2)
                ->where('matriz_id', $idMatriz)
                ->where('padre_id', $padre_id)->count();

            $letra = $sigla . ($validarCantidadItems + 1);

            $modulo = new Modulo;
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            $modulo->nivel = $nivel;
            $modulo->padre_id = $padre_id;
            $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            $modulo->matriz_id = $idMatriz;
            $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            $validarSigla = Modulo::where('sigla', $letra)
                ->where('nivel', 2)
                ->where('matriz_id', $idMatriz)
                ->where('padre_id', $padre_id)->count();

            if ($validarSigla > 0) {
                $validarCantidadItems = Modulo::where('nivel', 2)
                    ->where('matriz_id', $idMatriz)
                    ->where('padre_id', $padre_id)->count();

                $letra = $sigla . ($validarCantidadItems + 1);
                $modulo->sigla = $letra;
            } else {
                $modulo->sigla = $letra;
            }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }
    public function updateItems(Request $request)
    {

        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $id = $formdata['id'];
        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $sigla = $formdata['sigla'];
        $padre_id = $formdata['padre_id'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,' . $id . ',id,padre_id,' . $padre_id . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {




            // $validarCantidadItems = Modulo::where('nivel', 2)
            //     ->where('matriz_id', $idMatriz)
            //     ->where('padre_id', $padre_id)->count();

            // $letra = $sigla . ($validarCantidadItems + 1);

            $modulo = Modulo::find($id);
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            // $modulo->nivel = $nivel;
            // $modulo->padre_id = $padre_id;
            // $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            // $modulo->matriz_id = $idMatriz;
            // $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            // $validarSigla = Modulo::where('sigla', $letra)
            //     ->where('nivel', 2)
            //     ->where('matriz_id', $idMatriz)
            //     ->where('padre_id', $padre_id)->count();

            // if ($validarSigla > 0) {
            //     $validarCantidadItems = Modulo::where('nivel', 2)
            //         ->where('matriz_id', $idMatriz)
            //         ->where('padre_id', $padre_id)->count();

            //     $letra = $sigla . ($validarCantidadItems + 1);
            //     $modulo->sigla = $letra;
            // } else {
            //     $modulo->sigla = $letra;
            // }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    public function crearSubItems(Request $request)
    {

        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $sigla = $formdata['sigla'];
        $padre_id = $formdata['padre_id'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,null,null,padre_id,' . $padre_id . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {




            $validarCantidadItems = Modulo::where('nivel', 3)
                ->where('matriz_id', $idMatriz)
                ->where('padre_id', $padre_id)->count();

            $letra = $sigla . "-" . ($validarCantidadItems + 1);

            $modulo = new Modulo;
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            $modulo->nivel = $nivel;
            $modulo->padre_id = $padre_id;
            $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            $modulo->matriz_id = $idMatriz;
            $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            $validarSigla = Modulo::where('sigla', $letra)
                ->where('nivel', 2)
                ->where('matriz_id', $idMatriz)
                ->where('padre_id', $padre_id)->count();

            if ($validarSigla > 0) {
                $validarCantidadItems = Modulo::where('nivel', 3)
                    ->where('matriz_id', $idMatriz)
                    ->where('padre_id', $padre_id)->count();

                $letra = $sigla . "-" . ($validarCantidadItems + 1);
                $modulo->sigla = $letra;
            } else {
                $modulo->sigla = $letra;
            }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }
    public function updateSubitem(Request $request)
    {

        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $id = $formdata['id'];
        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $sigla = $formdata['sigla'];
        $padre_id = $formdata['padre_id'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,' . $id . ',id,padre_id,' . $padre_id . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {




            // $validarCantidadItems = Modulo::where('nivel', 3)
            //     ->where('matriz_id', $idMatriz)
            //     ->where('padre_id', $padre_id)->count();

            // $letra = $sigla . "-" . ($validarCantidadItems + 1);

            $modulo =  Modulo::find($id);
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            // $modulo->nivel = $nivel;
            // $modulo->padre_id = $padre_id;
            // $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            // $modulo->matriz_id = $idMatriz;
            // $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            // $validarSigla = Modulo::where('sigla', $letra)
            //     ->where('nivel', 2)
            //     ->where('matriz_id', $idMatriz)
            //     ->where('padre_id', $padre_id)->count();

            // if ($validarSigla > 0) {
            //     $validarCantidadItems = Modulo::where('nivel', 3)
            //         ->where('matriz_id', $idMatriz)
            //         ->where('padre_id', $padre_id)->count();

            //     $letra = $sigla . "-" . ($validarCantidadItems + 1);
            //     $modulo->sigla = $letra;
            // } else {
            //     $modulo->sigla = $letra;
            // }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $usuario_id = Session::get('id_usuario_calidad_cos'); //ID USUARIO SESSION

        $formdata = $request->dataform;
        $idMatriz = $request->idMatriz;


        $nombre = $formdata['nombre'];
        $peso = $formdata['peso'];
        $premium = $formdata['premium'];
        $nivel = $formdata['nivel'];
        $activo = $formdata['activo'];
        $error_critico = $formdata['error_critico'];


        $rules = [
            'dataform.nombre' => 'required|unique:modulos,nombre,' . $id . ',id,matriz_id,' . $idMatriz . '|min:3|max:150',
        ];
        $messages = [
            'dataform.nombre.required' => 'El campo NOMBRE es requerido.',
            'dataform.nombre.unique' => 'Ya existe un módulo con este nombre.',
            'dataform.nombre.min' => 'El campo NOMBRE debe tener mínimo :min caracteres.',
            'dataform.nombre.max' => 'El campo NOMBRE debe tener máximo :max caracteres.'
        ];

        $this->validate($request, $rules, $messages);

        DB::beginTransaction();
        try {


            // $letra = $this->consultarSigla($idMatriz);

            $modulo =  Modulo::find($id);
            $modulo->nombre = $nombre;
            $modulo->peso = $peso;
            $modulo->premium = $premium;
            $modulo->nivel = $nivel;
            $modulo->activo = $activo;
            $modulo->error_critico = $error_critico;
            // $modulo->matriz_id = $idMatriz;
            // $modulo->usuario_creador_id = $usuario_id;
            $modulo->usuario_actualizacion_id = $usuario_id;

            // $validarSigla = Modulo::where('nivel', 1)
            //     ->where('sigla', $letra)
            //     ->where('matriz_id', $idMatriz)->count();
            // if ($validarSigla > 0) {
            //     $letra = $this->consultarSigla($idMatriz);
            //     $modulo->sigla = $letra;
            // } else {
            //     $modulo->sigla = $letra;
            // }

            $modulo->save();


            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];


            DB::commit();
            return $data_return;
        } catch (\Exception $e) {
            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modulo =  Modulo::find($id);
        $modulo->activo = !$modulo->activo;
        $modulo->save();
    }
}
