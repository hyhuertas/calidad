<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VozClientePregunta extends Model
{
    protected $table = 'voz_cliente_preguntas';

    protected $fillable = ['tipo_pregunta_id', 'tipo_pregunta_item_id', 'pregunta', 'requerida', 'otra_respuesta', 'user_id', 'activo'];


    protected $appends = ['dialog', 'respuesta_usuario', 'respuesta_usuario_otra', 'respuesta_usuario_otra_enable'];

    public function getDialogAttribute()
    {
        return null;
    }
    public function getRespuestaUsuarioAttribute()
    {
        return null;
    }
    public function getRespuestaUsuarioOtraAttribute()
    {

        if ($this->tipo_pregunta_id == 2) {
            $otraRespuesta = array_search(1, array_column(json_decode(json_encode($this->opcionesRespuestaFormPreguntaActivos), TRUE), 'otra_respuesta'));
            //    array_search(1, array_column($this->opcionesRespuestaFormPreguntaActivos, 'otra_respuesta'));

            if ($otraRespuesta) {
                return null;
            }
        }
        return false;
    }
    public function getRespuestaUsuarioOtraEnableAttribute()
    {

        return false;
    }




    public function tipoPregunta()
    {
        return $this->belongsTo('App\Models\TipoPregunta',  'tipo_pregunta_id', 'id');
    }
    public function opcionesRespuestaFormPregunta()
    {
        return $this->hasMany('App\Models\VozClientePreguntaItem',  'voz_cliente_pregunta_id', 'id')->where('otra_respuesta', false);
    }
    public function opcionesRespuestaFormPreguntaActivos()
    {
        return $this->hasMany('App\Models\VozClientePreguntaItem',  'voz_cliente_pregunta_id', 'id')->where('activo', true)->orderByRaw('FIELD(nombre, "Otra (o)")');
    }
    public function rango()
    {
        return $this->belongsTo('App\Models\TipoPreguntaItem',  'tipo_pregunta_item_id', 'id');
    }
}
