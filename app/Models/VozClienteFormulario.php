<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VozClienteFormulario extends Model
{
    protected $table = 'voz_cliente_formularios';

    protected $fillable = ['nombre', 'user_id', 'activo'];

    public function numeroPreguntas()
    {
        return $this->hasMany('App\Models\VozClienteFormularioPregunta', 'voz_cliente_formulario_id', 'id');
    }
    public function preguntasAcivas()
    {
        return $this->hasMany('App\Models\VozClienteFormularioPregunta', 'voz_cliente_formulario_id', 'id')->with('pregunta.opcionesRespuestaFormPreguntaActivos', 'pregunta.rango')
        ->whereHas('pregunta', function  ($q) {
            $q->where('activo', true);
        });
    }
}
