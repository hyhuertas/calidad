<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
    protected $table     = 'encuestas';
    protected $fillable  = ['nombre', 'descripcion', 'usuario_creador_id', 'activo'];

}
