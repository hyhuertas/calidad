<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaniaAppMaster extends Model
{
    protected $table = 'crm_masterclaro.modulos';

    protected $primaryKey = 'id_modulo';

    protected $appends = ['grupo'];

    public function getGrupoAttribute() 
    {  
        return 'Master';
    }

}
