<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClasificacionItem extends Model
{
    protected $table     = 'clasificacion_items';
    protected $fillable  = ['nombre', 'nivel', 'padre_id', 'clasificacion_id', 'activo', 'usuario_creador_id'];

    public function clasificacionItem()
    {
       // return $this->hasMany('App\Models\ClasificacionItem', 'clasificacion_id')->whereNull('padre_id');
    }

    public function maxNiveles()
    {
      //  return $this->hasOne('App\Models\ClasificacionItem', 'clasificacion_id', 'id')
       // ->latest('nivel');
    }

}
