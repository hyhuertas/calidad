<?php

namespace App\Models\Excel;

use App\Models\MatrizAuditoria;
use App\Models\MatrizAuditoriaItem;
use App\Models\MatrizAuditor;
use App\Models\Supervisor;
use App\Models\Matriz;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
use Session;

class ExporteReporte implements FromView
{
    public function __construct(string $fechaInicial, string $fechaFinal, $id, string $id_matriz, string $clave)
    {
        $this->fechaInicio = $fechaInicial;
        $this->fechaFinal = $fechaFinal;
        $this->id = $id;
        $this->id_matriz = $id_matriz;
        $this->clave = $clave;
        // return $this->getData();
    }
    /* funcion para el exporte de la data a los distinton informes y reportes */
    // public function GetData()
    // {

    // }

    /**
     * @return View
     */

    public function view(): View
    {

        $id = $this->id;
        $desde = $this->fechaInicio . ' 00:00:00';
        $hasta = $this->fechaFinal . ' 23:59:59';
        $idmatriz = $this->id_matriz;
        $modulo = $this->clave;

        $rol = Session::get('numero_rol_calidad_cos');
        $crm = Session::get('id_usuario_calidad_cos');

        /*         $auditor = MatrizAuditor::where('matriz_id', $idmatriz)->pluck('usuario_id');//busqueda para el supervisor de la matriz
        $auditor_crm = ['crm' => $auditor[0]];

        $supervisor = Supervisor::where('id_usuario', $crm)->where('id_modulo', config('app.modulo'))->pluck('numero_rol');
        $auditor_id = ['rol_id' => $supervisor[0]];

        */
        $gestion = MatrizAuditoria::with(
            'itemModulos',
            'itemItems',
            'itemSubitems',
            'detalleMatriz',
            'encuestaGenerada.preguntaDetalle',
            'encuestaGenerada.respuestSelect'
        )->where('created_at', '>=', $desde)
            ->where('created_at', '<=', $hasta);

        $roles = ['1', '4', '5'];

        if (in_array($rol, $roles)) {
            if ($rol == 1) { //DESCARGA PARA EL CREADOR DE MATRICES
                if ($id == 1) {
                    $gestion = $gestion->where('matriz_id', $idmatriz)->get();
                }
            }

            if ($rol == 4 || $rol == 5) { //DESCARGA PARA EL AUDITOR Y EL SUPERVISOR
                if ($id == 1) {
                    $gestion = $gestion->where('matriz_id', $idmatriz)->where('auditor_id', $crm)->get();
                }
            }
        } else {
            return redirect('/404');
        }
        //caberas exporte
        $this->cabeceras = Matriz::with(['modulos.itemsModulo.subItems' => function ($query) {
            $query->where('activo', 1);
        }, 'modulos.itemsModulo' => function ($query) {
            $query->where('activo', 1);
        }, 'modulos' => function ($query) {
            $query->where('activo', 1);
        }])->where('id', $idmatriz)->first();
        $this->gestiones = $gestion;


        //dd(json_encode($this->cabeceras));


        // 2 modulo obj item ->subitem


         //dd($id);
                
        // $id = $this->id;
        if ($id == 1) {
            return view('reportes.excel-informe-general', [
                'gestiones' => ($this->gestiones),
                'cabeceras' => ($this->cabeceras)
            ]);
        }
        if ($id == 2) {
            return view('reportes.excel-informe-parcial', [
                'gestiones' => $this->gestiones,
                'cabeceras' => $this->cabeceras
            ]);
        }
    }
}
