<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Matriz extends Model
{
    protected $table     = 'matrices';
    protected $fillable  = ['nombre', 'tipo_matriz_id', 'asociacion_crm', 'grupo', 'crm_id', 'rango_total_id', 'tipo_peso_id', 'usuario_creador_id', 'usuario_actualizacion_id', 'tiene_encuesta', 'encuesta_id', 'activo'];


    protected $appends = ['valor_peso_validacion', 'nombre_campania', 'seleccion_campaign'];

    public function getSeleccionCampaignAttribute()
    {
        if ($this->crm_id) {
            return [
                "grupo" => $this->grupo,
                "id" => $this->crm_id,
                "name" => $this->nombre_campania

            ];
        } else {
            return null;
        }
    }
    public function getNombreCampaniaAttribute()
    {
        $nombre = 'Sin campaña';
        if ($this->grupo == 'Mios' && isset($this->crm_id)) {

            $nombreCampania = MiosCampaign::where('id', $this->crm_id)->select('name', 'id')->first();
            if ($nombreCampania) {
                $nombre = $nombreCampania->name;
            }
        } else if ($this->grupo == 'Master' && isset($this->crm_id)) {

            $nombreCampania = CampañaAppMaster::where('id_modulo', $this->crm_id)
                ->select('descripcion_modulo AS name', 'id_modulo AS id')->first();

            if ($nombreCampania) {
                $nombre = $nombreCampania->name;
            }
        }

        return $nombre;
    }

    public function getValorPesoValidacionAttribute()
    {
        $peso = [];
        $min = 0;
        $max = 0;

        if ($this->rango_total_id == 5) { //de 0 a 5
            $max = 5;
        } elseif ($this->rango_total_id == 6) { // de 0 a 100
            $max = 100;
        } elseif ($this->rango_total_id == 7) { // de 0 a 130
            $max = 130;
        } elseif ($this->rango_total_id == 24) { // de 1 a 5
            $min = 1;
            $max = 5;
        }

        return $peso = [
            "minimo" => $min,
            "maximo" => $max,
        ];
    }

    public function tipoMatriz()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'tipo_matriz_id');
    }


    public function formuarioEncuesta()
    {
        return $this->belongsTo('App\Models\VozClienteFormulario', 'encuesta_id');
    }

    public function auditores()
    {
        return $this->hasMany('App\Models\MatrizAuditor', 'matriz_id')->select('usuario_id', 'matriz_id');
    }
    public function supervisores()
    {
        return $this->hasMany('App\Models\Grupo', 'matriz_id')->select('supervisor_id', 'matriz_id');
    }

    public function modulos()
    {
        // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');
        $rolCalidad = Session::get('numero_rol_calidad_cos');

        $item =  $this->hasMany('App\Models\Modulo', 'matriz_id')->where('nivel', 1);
        if ($rolCalidad == 4 || $rolCalidad == 5) {
            $item = $item->where('activo', 1);
        }
        $item = $item->orderBy('id', 'ASC');

        return $item;
    }

    public function campana()
    {
        return $this->belongsTo('App\Models\CampañaAppMaster', 'crm_id', 'id_modulo');
    }
}
