<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VozClientePreguntaItem extends Model
{
    protected $table = 'voz_cliente_preguntas_items';

    protected $fillable = ['voz_cliente_pregunta_id', 'nombre', 'otra_respuesta', 'activo'];

}
