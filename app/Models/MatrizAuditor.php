<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrizAuditor extends Model
{
    protected $table     = 'matriz_auditores';
    protected $fillable  = ['matriz_id', 'usuario_id'];

  
}
