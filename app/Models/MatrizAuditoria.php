<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrizAuditoria extends Model
{
    protected $table     = 'matriz_auditorias';
    protected $fillable  = ['id', 'asesor_id', 'cedula_asesor', 'fecha_llamada', 'tlf_auditados', 'hora_contacto', 'ucid', 'grabacion_minutos', 'grabacion_segundos', 'tipo_monitoreo_id', 'tipo_servicio_id', 'tipo_auditoria_id', 'solucion_primer_contacto_id', 'razon_solucion_id', 'porque_no', 'ojt_id', 'seleccionar_dia', 'matriz_id', 'rol_auditor', 'auditor_id', 'observacion_auditor', 'total_punaje', 'observacion_encuesta', 'auditor_retroalimentacion_id', 'supervisor_retroalimentacion_id', 'fecha_retroalimentacion_auditor', 'fecha_retroalimentacion_supervisor', 'retroalimentacion_auditor', 'compromiso_calidad', 'retroalimentacion_supervisor', 'fecha_compromiso', 'compromiso', 'encuesta_id', 'maximo_puntaje', 'created_at', 'updated_at'];

    public function tipoAudioria()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'tipo_auditoria_id');
    }
    public function detalleMatriz()
    {
        return $this->belongsTo('App\Models\Matriz', 'matriz_id');
    }
    public function detalleAsesor()
    {
        return $this->belongsTo('App\Models\UsuarioAppMaster', 'asesor_id');
    }
    public function detalleAuditor()
    {
        return $this->belongsTo('App\Models\UsuarioAppMaster', 'auditor_id');
    }
    public function itemsAfectados()
    {
        return $this->hasMany('App\Models\MatrizAuditoriaItem', 'matriz_auditoria_id')->with('detalleItem')->where('padre_id', null);
    }
    public function items()
    {
        return $this->hasMany('App\Models\MatrizAuditoriaItem', 'matriz_auditoria_id')
            ->whereHas('detalleItem', function ($q) {
                $q->where('activo', 1);
            });
    }
    public function itemModulos()
    {
        return $this->items()->with('detalleItem')->where('padre_id', null)->orderBy('modulo_id', 'ASC');
    }
    public function itemItems()
    {
        return $this->items()->with('detalleItem')->where('nivel', 2)->orderBy('modulo_id', 'ASC');
    }
    public function itemSubitems()
    {
        return $this->items()->with('detalleItem')->where('nivel', 3)->orderBy('modulo_id', 'ASC');
    }
    public function detalleMonitoreo()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'tipo_monitoreo_id');
    }

    public function detalleServicio()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'tipo_servicio_id');
    }

    public function solucionContacto()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'solucion_primer_contacto_id');
    }

    public function detalleOjt()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'ojt_id');
    }

    public function detalleRazon()
    {
        return $this->belongsTo('App\Models\ClasificacionItem', 'razon_solucion_id');
    }

    public function itemsA()
    {
        return $this->hasMany('App\Models\MatrizAuditoriaItem', 'matriz_auditoria_id')->with('detalleItem')->where('padre_id', !null);
    }

    public function encuestaGenerada()
    {
        return $this->hasMany('App\Models\VozClienteRespuesta', 'auditoria_id', 'id');
    }
}
