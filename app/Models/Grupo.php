<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Matriz;

class Grupo extends Model
{
    protected $table = 'grupos';

    protected $fillable = ['nombre','matriz_id','supervisor_id','usuario_creador_id', 'correo_jefe_opraciones', 'correo_supervisor', 'activo',];

    public function matriz()
    {
        return $this->belongsTo(Matriz::class, 'matriz_id','id');
    }
    // public function totalAsesoressss()
    // {
    //     return $this->hasMany('App\Models\GrupoUsuario', 'grupo_id')->with('auditorias');
    // }
    public function asesoresGrupo()
    {
        return $this->hasMany('App\Models\GrupoUsuario', 'grupo_id');
    }
    public function totalAuditorias()
    {
        return $this->hasMany('App\Models\MatrizAuditoria', 'matriz_id', 'matriz_id');
    }
    public function totalAuditoriasRetroalimentadas()
    {
        return $this->hasMany('App\Models\MatrizAuditoria', 'matriz_id', 'matriz_id')->where(function ($query) {
            $query->whereNotNull('auditor_retroalimentacion_id')
            ->orWhereNotNull('supervisor_retroalimentacion_id');
        });
    }
    public function totalAuditoriasNoRetroalimentadas()
    {
        return $this->hasMany('App\Models\MatrizAuditoria', 'matriz_id', 'matriz_id')->whereNull('auditor_retroalimentacion_id')->whereNull('supervisor_retroalimentacion_id');
    }
}
