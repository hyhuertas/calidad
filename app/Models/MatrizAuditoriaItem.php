<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrizAuditoriaItem extends Model
{
    protected $table     = 'matriz_auditoria_items';
    protected $fillable  = ['matriz_auditoria_id', 'modulo_id', 'nivel', 'padre_id', 'cumple', 'no_cumple', 'no_aplica', 'si', 'no', 'tipo_matriz_id', 'puntaje', 'puntaje_obtenido'];


    
    protected $appends = ['type_select'];

    public function getTypeSelectAttribute() 
    {  
        $valor = 'no_aplica';
        if($this->cumple == 1){
            $valor = 'cumple';
        } else if($this->cumple == 1){
            $valor = 'cumple';
        }
        else if($this->no_cumple == 1){
            $valor = 'no_cumple';
        }
        else if($this->si == 1){
            $valor = 'si';
        }
        else if($this->no == 1){
            $valor = 'no';
        }
        else if($this->no_aplica == 1){
            $valor = 'no_aplica';
        }
        else {
            $valor = '-';
        }
        return $valor;
    }


    public function items()
    {
        return $this->hasMany('App\Models\MatrizAuditoriaItem', 'padre_id', 'modulo_id')->with('detalleItem')->where('nivel', 2)->orderBy('modulo_id', 'ASC');
    }
    public function subitems()
    {
        return $this->hasMany('App\Models\MatrizAuditoriaItem', 'padre_id', 'modulo_id')->with('detalleItem')->where('nivel', 3)->orderBy('modulo_id', 'ASC');
    }

    public function detalleItem()
    {
        return $this->belongsTo('App\Models\Modulo', 'modulo_id');
    }

}
