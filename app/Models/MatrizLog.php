<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatrizLog extends Model
{
    protected $table     = 'matrices_logs';
    protected $fillable  = ['nombre', 'tipo_matriz_id', 'asociacion_crm', 'crm_id', 'rango_total_id', 'tipo_peso_id', 'usuario_creador_id', 'usuario_actualizacion_id', 'tiene_encuesta', 'encuesta_id', 'activo'];

}
