<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VozClienteFormularioPregunta extends Model
{
    protected $table = 'voz_cliente_formularios_preguntas';

    protected $fillable = ['voz_cliente_pregunta_id', 'voz_cliente_formulario_id'];

    public function pregunta()
    {
        return $this->belongsTo('App\Models\VozClientePregunta',  'voz_cliente_pregunta_id', 'id')->where('activo', true);
    }
}
