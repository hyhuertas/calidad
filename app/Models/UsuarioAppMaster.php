<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioAppMaster extends Model
{
    protected $table = 'crm_masterclaro.permisos_modulos_usuarios_all';

    protected $primaryKey = 'id_usuario';

    public function usuarioApp()
    {
        return $this->belongsTo('App\Models\AsesorGrupo','id_usuario','id_usuario');
    }
}
