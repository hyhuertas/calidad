<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    protected $table = 'crm_masterclaro.roles';

    protected $fillable = ['id_usuario', 'id_modulo', 'numero_rol'];


    public function usuario()
    {
        return $this->belongsTo('App\Models\AsesorGrupo', 'id_usuario', 'id_usuario');
    }
}
