<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session; 

class Modulo extends Model
{
    protected $i     = 0;
    protected $table     = 'modulos';
    protected $fillable  = ['nombre', 'peso', 'premium', 'sigla', 'nivel', 'padre_id', 'activo', 'usuario_creador_id', 'usuario_actualizacion_id', 'matriz_id', 'error_critico'];
    

    protected $appends = ['type_select'];

    public function getTypeSelectAttribute() 
    {  
        return null;
    }

    public function itemsModulo()
    {
          // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');
        $rolCalidad = Session::get('numero_rol_calidad_cos');

        $item =  $this->hasMany('App\Models\Modulo', 'padre_id')->with('subItems')->where('nivel', 2);
        if ($rolCalidad == 4 || $rolCalidad == 5) {
            $item = $item->where('activo', 1);
        }
        $item = $item->orderBy('id', 'ASC');
        return $item;

    }
    public function subItems()
    {
        $rolCalidad = Session::get('numero_rol_calidad_cos');

        $item =  $this->hasMany('App\Models\Modulo', 'padre_id')->where('nivel', 3);
        if ($rolCalidad == 4 || $rolCalidad == 5) {
            $item = $item->where('activo', 1);
        }
        $item = $item->orderBy('id', 'ASC');
        return $item;
        
    }
 
}
