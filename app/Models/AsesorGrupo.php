<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsesorGrupo extends Model
{
    protected $table = "crm_masterclaro.usuarios";

    protected $primaryKey = 'id_usuario';

    protected $fillable = ['cedula_usuario','nombre_usuario','apellido_usuario','fecha_creacion_usuario'];
}
