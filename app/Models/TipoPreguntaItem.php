<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPreguntaItem extends Model
{
    protected $table = 'tipo_preguntas_items';

    protected $fillable = ['tipo_pregunta_id', 'nombre', 'regla', 'activo'];

}
