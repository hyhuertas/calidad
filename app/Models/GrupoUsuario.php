<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoUsuario extends Model
{
    protected $table = 'grupo_usuarios';

    protected $fillable = ['grupo_id', 'usuario_id'];

    public function asesores()
    {
        return $this->hasMany('App\Models\AsesorGrupo', 'id_usuario','usuario_id');
    }
    public function detalleAsesor()
    {
        return $this->belongsTo('App\Models\UsuarioAppMaster', 'usuario_id');
    }

}
