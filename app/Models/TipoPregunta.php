<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPregunta extends Model
{
    protected $table = 'tipo_preguntas';

    protected $fillable = ['nombre', 'activo'];

    public function itemsTipoPregunta()
    {
          // Session::get('id_usuario_calidad_cos');
        // Session::get('numero_rol_calidad_cos');

    
        return  $this->hasMany('App\Models\TipoPreguntaItem', 'tipo_pregunta_id')->where('activo', true);

    }

}
