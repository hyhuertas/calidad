<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VozClienteRespuesta extends Model
{
    protected $table = 'voz_cliente_respuestas';

    protected $fillable = ['auditoria_id', 'user_id', 'voz_cliente_pregunta_id', 'voz_cliente_formulario_id', 'respuesta', 'respuesta_id'];


    public function preguntaDetalle()
    {
        return $this->belongsTo('App\Models\VozClientePregunta', 'voz_cliente_pregunta_id');
    }
    public function respuestSelect()
    {
        return $this->belongsTo('App\Models\VozClientePreguntaItem', 'respuesta_id');
    }
}
