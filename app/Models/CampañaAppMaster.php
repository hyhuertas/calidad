<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampañaAppMaster extends Model
{
    protected $table = 'crm_masterclaro.modulos';   

    protected $fillable = ['id_modulo', 'descripcion_modulo', 'tipoModulo'];
}
