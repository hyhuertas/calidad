<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiosCampaign extends Model
{
    protected $connection = 'mios_connection';

    protected $table = "campaigns";

    protected $fillable = ['name', 'is_active'];

    protected $appends = ['grupo'];

    public function getGrupoAttribute() 
    {  
        return 'Mios';
    }
}
