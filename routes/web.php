<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsuarioController@validarAppmaster');



Route::get('/adm/calidad', function () {
    return view('admin');
});
Route::get('/adm/voz-cliente', function () {
    return view('admin');
});
Route::get('/adm/voz-cliente/formulario', function () {
    return view('admin');
});
Route::get('/adm/voz-cliente/formulario/{idformulario}', function () {
    return view('admin');
});

Route::get('/informes', function () {
    return view('informes');
});

Route::get('/admin', function () {
    return view('admin');
});
Route::get('/admin/matrizconfig/{idmatriz}', function () {
    return view('admin');
});

Route::get('/auditor', function () {
    return view('auditor');
});
Route::get('/auditor/matrizauditar/{idmatriz}', function () {
    return view('auditor');
});
Route::get('/auditor/matrizretroalimentar/{idmatriz}', function () {
    return view('auditor');
});

Route::get('/control', function () {
    return view('control');
});

Route::get('/control/campaña/{matriz_id}', function () {
    return view('control');
});
Route::get('/control/campaña', function () {
    return view('control');
});

Route::get('/asesor', function () {
    return view('asesor');
});

// Route::get('/supervisor', function () {
//     return view('supervisor');
// });
Route::get('/supervisor', function () {
    return view('supervisor');
});
Route::get('/supervisor/matrizauditar/{idmatriz}', function () {
    return view('supervisor');
});
Route::get('/supervisor/matrizretroalimentar/{idmatriz}', function () {
    return view('supervisor');
});
Route::get('/supervisor/verauditorias/{idmatriz}', function () {
    return view('supervisor');
});
//RUTA SUPERVISOR DESCARGAR REPORTE DE DICHA MATRIZ
Route::get('/supervisor/informes/{idmatriz}', function () {
    return view('supervisor');
});

//RUTA AUDITOR DESCARGAR REPORTE DE DICHA MATRIZ
Route::get('/auditor/informes/{idmatriz}', function () {
    return view('auditor');
});

//RUTA ADMIN DESCARGAR REPORTE DE DICHA MATRIZ
Route::get('/admin/informes/{idmatriz}', function () {
    return view('admin');
});

//CONTROL
Route::post('/countGruposSupervisor', 'GrupoController@countGruposSupervisor');//trae todos los grupos de una matriz
Route::get('/grupos/{id}', 'GrupoController@grupos');//trae todos los grupos de una matriz
Route::post('/crear-grupo', 'GrupoController@crearGrupo');//crea grupos
Route::post('/eliminar_grupo', 'GrupoController@eliminargrupo');//elimina grupos
Route::post('/buscar-asesor', 'GrupoController@busquedaAsesor');//busca asesor por cedula y matriz
Route::post('/agregar-asesor', 'GrupoController@agregarAsesor');//agrega asesor
Route::post('/eliminarUsuarioGrupo', 'GrupoController@eliminarAsesor');//elimina asesor del grupo
Route::post('/misAsesores', 'GrupoController@misAsesores');//busqueda de los asesores del grupo
Route::get('/supervisores', 'GrupoController@supervisores');//busqueda de supervisores para asignarlos al grupo

//ASESOR
Route::get('/auditorias', 'AsesorController@auditorias');//trae las auditorias del asesor para llenar la tabla de auditorias.
Route::get('/traerAuditoriasAsesor', 'AsesorController@auditoriasAsesor');//trae las auditorias del trimestre del hoy y del mes del asesor.
Route::put('/compromiso', 'AsesorController@compromiso');//guarda el compromiso del asesor despues de una auditoria
Route::post('/asesorRetroalimentar', 'AsesorController@asesorRetroalimentar'); //busca el asesor con los items

//INFORMES
Route::post('/descargarInforme', 'InformeController@descargarExcel');//descarga el informe
Route::post('/matrizActiva/{id}', 'InformeController@matrizActiva');//verificacion matriz activa

Route::post('/detalleGruposCalidad', 'AuditoriaController@detalleGruposCalidad'); //CREAR NUEVOS MÓDULOS
Route::post('/buscarGruposCalidad', 'AuditoriaController@buscarGruposCalidad'); //CREAR NUEVOS MÓDULOS
Route::resource('/crudAuditoria', 'AuditoriaController'); //CREAR NUEVOS MÓDULOS
Route::post('/crudRetroalimentacion', 'AuditoriaController@guardarRetroalimentacion'); //CREAR NUEVOS MÓDULOS
Route::post('/detalleAsesorRetroalimentar', 'AuditoriaController@detalleAsesorRetroalimentar'); //BUSCAR ASESORES POR MATRIZ PARA RETROALIMENTAR
Route::post('/buscarAsesorRetroalimentar', 'AuditoriaController@buscarAsesorRetroalimentar'); //BUSCAR ASESORES POR MATRIZ PARA RETROALIMENTAR
Route::post('/buscarAsesorAuditoria', 'AuditoriaController@buscarAsesorAuditoria'); //BUSCAR ASESOR PARA SUPERVIZOR
Route::post('/getAsesoresMatriz', 'AuditoriaController@getAsesoresMatriz'); //ASESORES PO MATRIZ

Route::resource('/crudModulo', 'ModuloController'); //CREAR NUEVOS MÓDULOS
Route::post('/crudModuloItems', 'ModuloController@crearItems'); //CREAR NUEVOS ITEMS
Route::post('/crudModuloItemsUpdate', 'ModuloController@updateItems'); //ACTUALIZAR ITEMS
Route::post('/crudModuloItemsSubItemsUpdate', 'ModuloController@updateSubitem'); //ACTUALIZAR SUB-ITEMS
Route::post('/crudModuloItemsSubItems', 'ModuloController@crearSubItems'); //CREAR NUEVOS SUB-ITEMS
Route::resource('/crudEncuesta', 'EncuestaController'); //MATRICES DE CALIDAD
Route::resource('/crudMatriz', 'MatrizController'); //MATRICES DE CALIDAD
Route::post('/gestionesMatriz', 'MatrizController@gestionesMatriz'); //MATRICES DE CALIDAD
Route::post('/listarItems', 'ClasificacionItemController@listarItems'); //LISTAR ITEMS PARA LOS SELECTS
Route::post('/listarCampaniaItems', 'CampaniaAppMasterController@listarCampania'); //LISTAR CAMPAÑAS ITEMS PARA LOS SELECTS
Route::get('/listarAuditoresItems', 'CampaniaAppMasterController@listarAuditores'); //LISTADO DE AUDITORES
Route::post('/guardarAuditores', 'CampaniaAppMasterController@guardarAuditores'); //ASIGNACION DE AUDITORES
Route::post('/buscarAsesor', 'CampaniaAppMasterController@buscarAsesor'); //USUARIO CON ROL ASESOR

/* VOZ CLIENTE */

Route::post('/validarDisponiblePregunta', 'VozClienteController@validarDisponiblePregunta'); //PARA ELIMINAR
Route::post('/validarDisponibleFormulario', 'VozClienteController@validarDisponibleFormulario');  //PARA ELIMINAR
Route::post('/getTiposPreguntas', 'VozClienteController@getTiposPreguntas'); 
Route::resource('/crudPreguntas', 'VozClientePreguntaController'); //MATRICES DE CALIDAD
Route::resource('/crudFormulario', 'VozClienteController'); //MATRICES DE CALIDAD
Route::get('/verMatricesAsignadas/{formulario_id}', 'VozClienteController@verMatricesAsignadas'); //MATRICES DE CALIDAD
/* VOZ CLIENTE */

// Route::get('/control', function () {
//     return view('control');
// });
// Route::get('/control/campaña', function () {
//     return view('control');
// });

// Route::get('/asesor', function () {
//     return view('asesor');
// });

// Route::get('/supervisor', function () {
//     return view('supervisor');
// });

